﻿CREATE DOMAIN domNombre VARCHAR(30);
CREATE DOMAIN domCorreo VARCHAR(40);
CREATE TABLE Usuario(
	correo domCorreo,				--correo electrónico
	nombre domNombre NOT NULL,		--nombre
	ap domNombre NOT NULL,			--apellido paterno
	am domNombre NOT NULL,			--apellido materno
	fecha DATE,						--fecha de nacimiento
	semestre INTEGER,				--semestre que cursa
	contrasena VARCHAR(32) NOT NULL,--contraseña
	historial BYTEA NOT NULL,		--historial académico
	credito INTEGER NOT NULL,		--cantidad de dinero
	PRIMARY KEY (correo)
);
CREATE TABLE Videojuego(
	id INTEGER,						--identificador entero
	nombre domNombre NOT NULL,		--nombre comercial
	fecha DATE,						--fecha de publicación
	descripcion TEXT NOT NULL,		--descripción del videojuego
	video BYTEA,					--video del juego
	desarrollador domNombre,		--nombre del desarrollador
	costo INTEGER NOT NULL,			--costo al público
	PRIMARY KEY (id)
);
CREATE TABLE Bajar(
	correoUsuario domCorreo,		--correo del usuario
	idVideojuego INTEGER,			--identificador del videojuego
	PRIMARY KEY (correoUsuario, idVideojuego),
	FOREIGN KEY (correoUsuario) REFERENCES Usuario(correo),
	FOREIGN KEY (idVideojuego) REFERENCES Videojuego(id)
);