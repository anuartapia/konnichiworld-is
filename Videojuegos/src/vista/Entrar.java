package vista;

import controlador.InicioSesion;
import java.io.IOException;
import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import modelo.Alumno;
import modelo.Desarrollador;

/**
 * Servlet que maneja las peticiones de entrar.jsp. Verifica con ayuda de
 * InicioSesion.java que los datos introducidos en entrar.jsp correspondan a un
 * usuario y en caso satisfactorio incializa la sesión HTTP con los datos
 * correspondentes.
 *
 * @author anuar
 */
@WebServlet(name = "Entrar", urlPatterns = {"/Entrar"})
public class Entrar extends HttpServlet {

    @PersistenceUnit(unitName = "VideojuegosPU")
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction utx;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String correo = request.getParameter("usuario");//obtiene el correo
        String password = request.getParameter("contrasena");//obtiene la contraseña
        HttpSession session;//nueva sesión
        InicioSesion is = new InicioSesion(utx, emf);//controlador
        switch (is.verificaUsuario(correo, password)) {
            case InicioSesion.INVALIDO://usuario inexistente
                response.sendRedirect("entrar.jsp");
                break;
            case InicioSesion.INCORRECTO: //contraseña incorrecta
                response.sendRedirect("entrar.jsp");
                break;
            case InicioSesion.OK://datos correctos
                session = request.getSession();//inicializa la sesión u obtiene una existente
                //asigna los atributos necesarios
                boolean esAlumno = is.esAlumno(correo);//determina si es un alumno
                if (esAlumno) {
                    Alumno a = is.obtenAlumno(correo);
                    session.setAttribute("alumno", a);
                } else {
                    Desarrollador d = is.obtenDesarrollador(correo);
                    session.setAttribute("desarrollador", d);
                }
                /* marca en la sesion si se trata de un alumno o no */
                session.setAttribute("esAlumno", Boolean.valueOf(esAlumno));
                response.sendRedirect("index.jsp");
                break;
            default:
                response.sendRedirect("index.jsp");
                break;
        }
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
