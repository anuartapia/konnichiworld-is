package vista;

import java.io.IOException;
import java.io.PrintWriter;
import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import modelo.Videojuego;
import modelo.VideojuegoJpaController;

/**
 * Servlet que maneja las peticiones de catalogo.jsp. Obtiene de la base de
 * datos el <code>Videojuego</code> que le indiquen y se lo pasa a
 * juego_detalle.jsp por medio de la sesión HTTP para que despliegue la
 * información.
 *
 * @author anuar
 */
public class DetalleJuegoIH extends HttpServlet {

    @PersistenceUnit(unitName = "VideojuegosPU")
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction utx;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        VideojuegoJpaController vjc = new VideojuegoJpaController(utx, emf);// controlador
        int idJuego = Integer.parseInt(request.getParameter("idjuego"));// identificador del videojuego
        Videojuego v = vjc.findVideojuego(idJuego); //videojuego
        HttpSession session = request.getSession(); //sesión activa
        session.setAttribute("juego", v); //asigna el videojuego como atributo de la sesión
        response.sendRedirect("detalle_juego.jsp"); //redirige a detalle:juego.jsp para que despliegue la información
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
