package vista;

import java.io.IOException;
import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;

import controlador.Descargador;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collection;
import javax.servlet.ServletOutputStream;
import modelo.Alumno;
import modelo.Videojuego;

/**
 * Servlet que maneja las peticiones de la vista detalle_juego.jsp. Se comunica
 * con el controlador Descargador para verificar si la sesión activa puede
 * iniciar la decarga de un Videojuego.
 *
 * @version 1 19 Abr 2015
 * @author anuar
 */
@WebServlet(name = "DescargaIH", urlPatterns = {"/DescargaIH"})
public class DescargaIH extends HttpServlet {

    @PersistenceUnit(unitName = "VideojuegosPU")
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction utx;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(); //sesion activa
        Alumno a = (Alumno) session.getAttribute("alumno"); //alumno al que pertenece la sesión
        /* si la sesión no pertenece a un usuario alumno tomamos el idAlumno como null */
        String idAlumno = (a == null) ? null : a.getCorreo();
        /* obtiene el identificador del videojuego a descargar */
        int idVideojuego = Integer.parseInt(request.getParameter("idjuego"));
        Descargador d = new Descargador(idAlumno, idVideojuego, emf, utx);//controlador
        /* pregunta si se puede relizar la descarga */
        switch (d.puedeDescargar()) {
            case Descargador.NO_EXISTE_VIDEOJUEGO: //no existe el videojuego
                response.sendRedirect("detalle_juego.jsp");
                return;
            case Descargador.NO_TIENE_CREDITO: //el usuario (alumno o visitante) no tiene credito
                response.sendRedirect("detalle_juego.jsp");
                return;
            case Descargador.OK: //puede realizar la descarga
                byte[] exe = d.obtenArchivo();//obtiene el archivo haciendo actualizaciones en la bd
                if (d.getAlumno() != null) {//actualiza los datos de la sesion, si es que es de un alumno
                    session.setAttribute("alumno", d.getAlumno());
                }
                /*Inicia la descarga*/
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition",
                        "attachment;filename=" + d.getVideojuego().getNombre().replace(' ', '_') + ".kw");
                StringBuilder sb = new StringBuilder("lo que sea");
                InputStream in = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
                ServletOutputStream out = response.getOutputStream();
                byte[] outputByte = new byte[4096];
                while (in.read(outputByte, 0, 4096) != -1) {
                    out.write(outputByte, 0, 4096);
                }
                in.close();
                out.flush();
                out.close();
                return;
            default:
                response.sendRedirect("index.jsp");
                return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
