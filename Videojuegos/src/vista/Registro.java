/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;


import controlador.InicioSesion;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import modelo.Alumno;
import modelo.AlumnoJpaController;


/**
 * Servlet que maneja las peticiones de la vista registro.jsp. Se comunica con el
 * controlador CreaUsuario para verificar los datos que se obtienen de
 * registro.jsp y asi dar de alta un nuevo usuario en la base de datos.
 * @author esteban
 */
@WebServlet(name = "RegistrarAlumno", urlPatterns = {"/RegistrarAlumno"})
public class Registro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            UserTransaction utx = null;
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("VideojuegosPU");

            AlumnoJpaController alumnoJpa = new AlumnoJpaController(utx, emf);
            Alumno a = alumnoJpa.findAlumno("correo");
            HttpSession session = request.getSession();
            
            if (a != null) {
                
                response.sendRedirect("registro.jsp");
                
                return;
            } else {
                session.setAttribute("error", "No se encontró alumno.");
                response.sendRedirect("registro.jsp");
                
                return;
            }

        } catch (Exception e) {
            response.sendRedirect("AlumnoFantasma.jsp");
            return;
        }
            
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
