/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anuar
 */
@Entity
@Table(name = "videojuego")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Videojuego.findAll", query = "SELECT v FROM Videojuego v"),
    @NamedQuery(name = "Videojuego.findById", query = "SELECT v FROM Videojuego v WHERE v.id = :id"),
    @NamedQuery(name = "Videojuego.findByNombre", query = "SELECT v FROM Videojuego v WHERE v.nombre = :nombre"),
    @NamedQuery(name = "Videojuego.findByFecha", query = "SELECT v FROM Videojuego v WHERE v.fecha = :fecha"),
    @NamedQuery(name = "Videojuego.findByDesarrollador", query = "SELECT v FROM Videojuego v WHERE v.desarrollador = :desarrollador"),
    @NamedQuery(name = "Videojuego.findByDescripcion", query = "SELECT v FROM Videojuego v WHERE v.descripcion = :descripcion"),
    @NamedQuery(name = "Videojuego.list", query = "SELECT v.id,v.nombre FROM Videojuego v WHERE lower (v.nombre) LIKE :letras"),
    @NamedQuery(name = "Videojuego.takeId", query = "SELECT Max(v.id) FROM Videojuego  v"),
    //@NamedQuery(name = "Videojuego.update", query = "UPDATE Videojuego set  nombre = :nombre , fecha = :fecha , desarrolldor = :desarrolldor , descripcion = :descripcion , costo = :costo where id = :id"),
    @NamedQuery(name = "Videojuego.findByIdNot", query = "SELECT v.id,v.nombre,v.fecha,v.desarrollador,v.descripcion,v.costo FROM Videojuego v WHERE v.id = :id"),
    @NamedQuery(name = "Videojuego.findByCosto", query = "SELECT v FROM Videojuego v WHERE v.costo = :costo")})
    
public class Videojuego implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "desarrollador")
    private String desarrollador;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "costo")
    private int costo;
    @Basic(optional = false)
    @Lob
    @Column(name = "ejecutable")
    private byte[] ejecutable;
    @JoinTable(name = "subir", joinColumns = {
        @JoinColumn(name = "id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "usuario", referencedColumnName = "usuario")})
    @ManyToMany
    private List<Desarrollador> desarrolladorList;
    @ManyToMany(mappedBy = "videojuegoList")
    private List<Alumno> alumnoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "videojuego")
    private List<Imagen> imagenList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "videojuego")
    private List<Video> videoList;

    public Videojuego() {
    }

    public Videojuego(Integer id) {
        this.id = id;
    }

    public Videojuego(Integer id, String nombre, String descripcion, int costo, byte[] ejecutable) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.costo = costo;
        this.ejecutable = ejecutable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDesarrollador() {
        return desarrollador;
    }

    public void setDesarrollador(String desarrollador) {
        this.desarrollador = desarrollador;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    public byte[] getEjecutable() {
        return ejecutable;
    }

    public void setEjecutable(byte[] ejecutable) {
        this.ejecutable = ejecutable;
    }

    @XmlTransient
    public List<Desarrollador> getDesarrolladorList() {
        return desarrolladorList;
    }

    public void setDesarrolladorList(List<Desarrollador> desarrolladorList) {
        this.desarrolladorList = desarrolladorList;
    }

    @XmlTransient
    public List<Alumno> getAlumnoList() {
        return alumnoList;
    }

    public void setAlumnoList(List<Alumno> alumnoList) {
        this.alumnoList = alumnoList;
    }

    @XmlTransient
    public List<Imagen> getImagenList() {
        return imagenList;
    }

    public void setImagenList(List<Imagen> imagenList) {
        this.imagenList = imagenList;
    }

    @XmlTransient
    public List<Video> getVideoList() {
        return videoList;
    }
    
    public void setVideoList(List<Video> videoList) {
        this.videoList = videoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Videojuego)) {
            return false;
        }
        Videojuego other = (Videojuego) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Videojuego[ id=" + id + " ]";
    }
    
}
