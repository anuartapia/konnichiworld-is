/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anuar
 */
@Entity
@Table(name = "imagen")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Imagen.findAll", query = "SELECT i FROM Imagen i"),
    @NamedQuery(name = "Imagen.findByIdvideojuego", query = "SELECT i FROM Imagen i WHERE i.imagenPK.idvideojuego = :idvideojuego"),
    @NamedQuery(name = "Imagen.findByIdimagen", query = "SELECT i FROM Imagen i WHERE i.imagenPK.idimagen = :idimagen"),
    @NamedQuery(name = "Imagen.findByRutaarchivo", query = "SELECT i FROM Imagen i WHERE i.rutaarchivo = :rutaarchivo")})
public class Imagen implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ImagenPK imagenPK;
    @Size(max = 500)
    @Column(name = "rutaarchivo")
    private String rutaarchivo;
    @JoinColumn(name = "idvideojuego", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Videojuego videojuego;

    public Imagen() {
    }

    public Imagen(ImagenPK imagenPK) {
        this.imagenPK = imagenPK;
    }

    public Imagen(int idvideojuego, int idimagen) {
        this.imagenPK = new ImagenPK(idvideojuego, idimagen);
    }

    public ImagenPK getImagenPK() {
        return imagenPK;
    }

    public void setImagenPK(ImagenPK imagenPK) {
        this.imagenPK = imagenPK;
    }

    public String getRutaarchivo() {
        return rutaarchivo;
    }

    public void setRutaarchivo(String rutaarchivo) {
        this.rutaarchivo = rutaarchivo;
    }

    public Videojuego getVideojuego() {
        return videojuego;
    }

    public void setVideojuego(Videojuego videojuego) {
        this.videojuego = videojuego;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imagenPK != null ? imagenPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Imagen)) {
            return false;
        }
        Imagen other = (Imagen) object;
        if ((this.imagenPK == null && other.imagenPK != null) || (this.imagenPK != null && !this.imagenPK.equals(other.imagenPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Imagen[ imagenPK=" + imagenPK + " ]";
    }
    
}
