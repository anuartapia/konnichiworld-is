/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import modelo.exceptions.IllegalOrphanException;
import modelo.exceptions.NonexistentEntityException;
import modelo.exceptions.PreexistingEntityException;
import modelo.exceptions.RollbackFailureException;

/**
 *
 * @author anuar
 */
public class AlumnoJpaController implements Serializable {

    public AlumnoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Alumno alumno) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (alumno.getVideojuegoList() == null) {
            alumno.setVideojuegoList(new ArrayList<Videojuego>());
        }
        if (alumno.getHistorialList() == null) {
            alumno.setHistorialList(new ArrayList<Historial>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Videojuego> attachedVideojuegoList = new ArrayList<Videojuego>();
            for (Videojuego videojuegoListVideojuegoToAttach : alumno.getVideojuegoList()) {
                videojuegoListVideojuegoToAttach = em.getReference(videojuegoListVideojuegoToAttach.getClass(), videojuegoListVideojuegoToAttach.getId());
                attachedVideojuegoList.add(videojuegoListVideojuegoToAttach);
            }
            alumno.setVideojuegoList(attachedVideojuegoList);
            List<Historial> attachedHistorialList = new ArrayList<Historial>();
            for (Historial historialListHistorialToAttach : alumno.getHistorialList()) {
                historialListHistorialToAttach = em.getReference(historialListHistorialToAttach.getClass(), historialListHistorialToAttach.getHistorialPK());
                attachedHistorialList.add(historialListHistorialToAttach);
            }
            alumno.setHistorialList(attachedHistorialList);
            em.persist(alumno);
            for (Videojuego videojuegoListVideojuego : alumno.getVideojuegoList()) {
                videojuegoListVideojuego.getAlumnoList().add(alumno);
                videojuegoListVideojuego = em.merge(videojuegoListVideojuego);
            }
            for (Historial historialListHistorial : alumno.getHistorialList()) {
                Alumno oldAlumnoOfHistorialListHistorial = historialListHistorial.getAlumno();
                historialListHistorial.setAlumno(alumno);
                historialListHistorial = em.merge(historialListHistorial);
                if (oldAlumnoOfHistorialListHistorial != null) {
                    oldAlumnoOfHistorialListHistorial.getHistorialList().remove(historialListHistorial);
                    oldAlumnoOfHistorialListHistorial = em.merge(oldAlumnoOfHistorialListHistorial);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findAlumno(alumno.getCorreo()) != null) {
                throw new PreexistingEntityException("Alumno " + alumno + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Alumno alumno) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Alumno persistentAlumno = em.find(Alumno.class, alumno.getCorreo());
            List<Videojuego> videojuegoListOld = persistentAlumno.getVideojuegoList();
            List<Videojuego> videojuegoListNew = alumno.getVideojuegoList();
            List<Historial> historialListOld = persistentAlumno.getHistorialList();
            List<Historial> historialListNew = alumno.getHistorialList();
            List<String> illegalOrphanMessages = null;
            for (Historial historialListOldHistorial : historialListOld) {
                if (!historialListNew.contains(historialListOldHistorial)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Historial " + historialListOldHistorial + " since its alumno field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Videojuego> attachedVideojuegoListNew = new ArrayList<Videojuego>();
            for (Videojuego videojuegoListNewVideojuegoToAttach : videojuegoListNew) {
                videojuegoListNewVideojuegoToAttach = em.getReference(videojuegoListNewVideojuegoToAttach.getClass(), videojuegoListNewVideojuegoToAttach.getId());
                attachedVideojuegoListNew.add(videojuegoListNewVideojuegoToAttach);
            }
            videojuegoListNew = attachedVideojuegoListNew;
            alumno.setVideojuegoList(videojuegoListNew);
            List<Historial> attachedHistorialListNew = new ArrayList<Historial>();
            for (Historial historialListNewHistorialToAttach : historialListNew) {
                historialListNewHistorialToAttach = em.getReference(historialListNewHistorialToAttach.getClass(), historialListNewHistorialToAttach.getHistorialPK());
                attachedHistorialListNew.add(historialListNewHistorialToAttach);
            }
            historialListNew = attachedHistorialListNew;
            alumno.setHistorialList(historialListNew);
            alumno = em.merge(alumno);
            for (Videojuego videojuegoListOldVideojuego : videojuegoListOld) {
                if (!videojuegoListNew.contains(videojuegoListOldVideojuego)) {
                    videojuegoListOldVideojuego.getAlumnoList().remove(alumno);
                    videojuegoListOldVideojuego = em.merge(videojuegoListOldVideojuego);
                }
            }
            for (Videojuego videojuegoListNewVideojuego : videojuegoListNew) {
                if (!videojuegoListOld.contains(videojuegoListNewVideojuego)) {
                    videojuegoListNewVideojuego.getAlumnoList().add(alumno);
                    videojuegoListNewVideojuego = em.merge(videojuegoListNewVideojuego);
                }
            }
            for (Historial historialListNewHistorial : historialListNew) {
                if (!historialListOld.contains(historialListNewHistorial)) {
                    Alumno oldAlumnoOfHistorialListNewHistorial = historialListNewHistorial.getAlumno();
                    historialListNewHistorial.setAlumno(alumno);
                    historialListNewHistorial = em.merge(historialListNewHistorial);
                    if (oldAlumnoOfHistorialListNewHistorial != null && !oldAlumnoOfHistorialListNewHistorial.equals(alumno)) {
                        oldAlumnoOfHistorialListNewHistorial.getHistorialList().remove(historialListNewHistorial);
                        oldAlumnoOfHistorialListNewHistorial = em.merge(oldAlumnoOfHistorialListNewHistorial);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = alumno.getCorreo();
                if (findAlumno(id) == null) {
                    throw new NonexistentEntityException("The alumno with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Alumno alumno;
            try {
                alumno = em.getReference(Alumno.class, id);
                alumno.getCorreo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The alumno with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Historial> historialListOrphanCheck = alumno.getHistorialList();
            for (Historial historialListOrphanCheckHistorial : historialListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Alumno (" + alumno + ") cannot be destroyed since the Historial " + historialListOrphanCheckHistorial + " in its historialList field has a non-nullable alumno field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Videojuego> videojuegoList = alumno.getVideojuegoList();
            for (Videojuego videojuegoListVideojuego : videojuegoList) {
                videojuegoListVideojuego.getAlumnoList().remove(alumno);
                videojuegoListVideojuego = em.merge(videojuegoListVideojuego);
            }
            em.remove(alumno);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Alumno> findAlumnoEntities() {
        return findAlumnoEntities(true, -1, -1);
    }

    public List<Alumno> findAlumnoEntities(int maxResults, int firstResult) {
        return findAlumnoEntities(false, maxResults, firstResult);
    }

    private List<Alumno> findAlumnoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Alumno.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Alumno findAlumno(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Alumno.class, id);
        } finally {
            em.close();
        }
    }

    public int getAlumnoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Alumno> rt = cq.from(Alumno.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
