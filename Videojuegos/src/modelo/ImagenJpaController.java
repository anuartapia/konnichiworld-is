/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import modelo.exceptions.NonexistentEntityException;
import modelo.exceptions.PreexistingEntityException;
import modelo.exceptions.RollbackFailureException;

/**
 *
 * @author anuar
 */
public class ImagenJpaController implements Serializable {

    public ImagenJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Imagen imagen) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (imagen.getImagenPK() == null) {
            imagen.setImagenPK(new ImagenPK());
        }
        imagen.getImagenPK().setIdvideojuego(imagen.getVideojuego().getId());
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Videojuego videojuego = imagen.getVideojuego();
            if (videojuego != null) {
                videojuego = em.getReference(videojuego.getClass(), videojuego.getId());
                imagen.setVideojuego(videojuego);
            }
            em.persist(imagen);
            if (videojuego != null) {
                videojuego.getImagenList().add(imagen);
                videojuego = em.merge(videojuego);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findImagen(imagen.getImagenPK()) != null) {
                throw new PreexistingEntityException("Imagen " + imagen + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Imagen imagen) throws NonexistentEntityException, RollbackFailureException, Exception {
        imagen.getImagenPK().setIdvideojuego(imagen.getVideojuego().getId());
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Imagen persistentImagen = em.find(Imagen.class, imagen.getImagenPK());
            Videojuego videojuegoOld = persistentImagen.getVideojuego();
            Videojuego videojuegoNew = imagen.getVideojuego();
            if (videojuegoNew != null) {
                videojuegoNew = em.getReference(videojuegoNew.getClass(), videojuegoNew.getId());
                imagen.setVideojuego(videojuegoNew);
            }
            imagen = em.merge(imagen);
            if (videojuegoOld != null && !videojuegoOld.equals(videojuegoNew)) {
                videojuegoOld.getImagenList().remove(imagen);
                videojuegoOld = em.merge(videojuegoOld);
            }
            if (videojuegoNew != null && !videojuegoNew.equals(videojuegoOld)) {
                videojuegoNew.getImagenList().add(imagen);
                videojuegoNew = em.merge(videojuegoNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ImagenPK id = imagen.getImagenPK();
                if (findImagen(id) == null) {
                    throw new NonexistentEntityException("The imagen with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ImagenPK id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Imagen imagen;
            try {
                imagen = em.getReference(Imagen.class, id);
                imagen.getImagenPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The imagen with id " + id + " no longer exists.", enfe);
            }
            Videojuego videojuego = imagen.getVideojuego();
            if (videojuego != null) {
                videojuego.getImagenList().remove(imagen);
                videojuego = em.merge(videojuego);
            }
            em.remove(imagen);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Imagen> findImagenEntities() {
        return findImagenEntities(true, -1, -1);
    }

    public List<Imagen> findImagenEntities(int maxResults, int firstResult) {
        return findImagenEntities(false, maxResults, firstResult);
    }

    private List<Imagen> findImagenEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Imagen.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Imagen findImagen(ImagenPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Imagen.class, id);
        } finally {
            em.close();
        }
    }

    public int getImagenCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Imagen> rt = cq.from(Imagen.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
