CREATE DOMAIN domNombre VARCHAR(50);
CREATE DOMAIN domCorreo VARCHAR(60);
CREATE TABLE Alumno(
    correo domCorreo,			--correo electrónico
    contrasena VARCHAR(32) NOT NULL,    --contraseña
    nombre domNombre NOT NULL,		--nombre
    ap domNombre NOT NULL,		--apellido paterno
    am domNombre NOT NULL,		--apellido materno
    semestre INTEGER,                   --semestre que cursa
    fecha DATE,				--fecha de nacimiento
    credito INTEGER NOT NULL,		--cantidad de dinero
    PRIMARY KEY (correo)
);
CREATE TABLE Videojuego(
    id INTEGER,				--identificador entero
    nombre domNombre NOT NULL,		--nombre comercial
    fecha DATE,				--fecha de publicación
    desarrollador domNombre,		--nombre del desarrollador
    descripcion TEXT NOT NULL,		--descripción del videojuego
    costo INTEGER NOT NULL,		--costo al público
    ejecutable BYTEA NOT NULL,          --archivo del juego
    PRIMARY KEY (id)
);
CREATE TABLE Desarrollador(
    usuario domNombre,                  --login
    nombre domNombre NOT NULL,		--nombre
    correo domCorreo NOT NULL,          --correo electrónico
    contrasena VARCHAR(32) NOT NULL,    --contraseña
    PRIMARY KEY (usuario)
);
CREATE TABLE Bajar(
    correo domCorreo,                   --correo del Alumno
    id INTEGER,         		--identificador del Videojuego
    PRIMARY KEY (correo, id),
    FOREIGN KEY (correo) REFERENCES Alumno(correo),
    FOREIGN KEY (id) REFERENCES Videojuego(id)
);
CREATE TABLE Subir(
    usuario domNombre,                  --login del Desarrollador
    id INTEGER,         		--identificador del Videojuego
    PRIMARY KEY (usuario, id),
    FOREIGN KEY (usuario) REFERENCES Desarrollador(usuario),
    FOREIGN KEY (id) REFERENCES Videojuego(id)
);
CREATE TABLE Historial(
    correo domCorreo,                   --correo del Alumno
    semestre INTEGER,                   --numero de semestre del historial
    archivo BYTEA,      		--archivo del historial académico
    PRIMARY KEY (correo, semestre, archivo),
    FOREIGN KEY (correo) REFERENCES Alumno(correo)
);
CREATE TABLE Video(
    idVideojuego INTEGER,               --identificador del videojuego
    idVideo INTEGER,                    --identificador de este video
    rutaArchivo VARCHAR(500),  		--ruta del archivo del video
    PRIMARY KEY (idVideojuego, idVideo),
    FOREIGN KEY (idVideojuego) REFERENCES Videojuego(id)
);
CREATE TABLE Imagen(
    idVideojuego INTEGER,               --identificador del videojuego
    idImagen INTEGER,                   --identificador de esta imagen
    rutaArchivo VARCHAR(500),  		--ruta del archivo de la imagen
    PRIMARY KEY (idVideojuego, idImagen),
    FOREIGN KEY (idVideojuego) REFERENCES Videojuego(id)
);