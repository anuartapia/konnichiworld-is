/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anuar
 */
@Entity
@Table(name = "historial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Historial.findAll", query = "SELECT h FROM Historial h"),
    @NamedQuery(name = "Historial.findByCorreo", query = "SELECT h FROM Historial h WHERE h.historialPK.correo = :correo"),
    @NamedQuery(name = "Historial.findBySemestre", query = "SELECT h FROM Historial h WHERE h.historialPK.semestre = :semestre")})
public class Historial implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HistorialPK historialPK;
    @JoinColumn(name = "correo", referencedColumnName = "correo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Alumno alumno;

    public Historial() {
    }

    public Historial(HistorialPK historialPK) {
        this.historialPK = historialPK;
    }

    public Historial(String correo, int semestre, byte[] archivo) {
        this.historialPK = new HistorialPK(correo, semestre, archivo);
    }

    public HistorialPK getHistorialPK() {
        return historialPK;
    }

    public void setHistorialPK(HistorialPK historialPK) {
        this.historialPK = historialPK;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historialPK != null ? historialPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Historial)) {
            return false;
        }
        Historial other = (Historial) object;
        if ((this.historialPK == null && other.historialPK != null) || (this.historialPK != null && !this.historialPK.equals(other.historialPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Historial[ historialPK=" + historialPK + " ]";
    }
    
}
