/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import modelo.exceptions.NonexistentEntityException;
import modelo.exceptions.PreexistingEntityException;
import modelo.exceptions.RollbackFailureException;

/**
 *
 * @author anuar
 */
public class HistorialJpaController implements Serializable {

    public HistorialJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Historial historial) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (historial.getHistorialPK() == null) {
            historial.setHistorialPK(new HistorialPK());
        }
        historial.getHistorialPK().setCorreo(historial.getAlumno().getCorreo());
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Alumno alumno = historial.getAlumno();
            if (alumno != null) {
                alumno = em.getReference(alumno.getClass(), alumno.getCorreo());
                historial.setAlumno(alumno);
            }
            em.persist(historial);
            if (alumno != null) {
                alumno.getHistorialList().add(historial);
                alumno = em.merge(alumno);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findHistorial(historial.getHistorialPK()) != null) {
                throw new PreexistingEntityException("Historial " + historial + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Historial historial) throws NonexistentEntityException, RollbackFailureException, Exception {
        historial.getHistorialPK().setCorreo(historial.getAlumno().getCorreo());
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Historial persistentHistorial = em.find(Historial.class, historial.getHistorialPK());
            Alumno alumnoOld = persistentHistorial.getAlumno();
            Alumno alumnoNew = historial.getAlumno();
            if (alumnoNew != null) {
                alumnoNew = em.getReference(alumnoNew.getClass(), alumnoNew.getCorreo());
                historial.setAlumno(alumnoNew);
            }
            historial = em.merge(historial);
            if (alumnoOld != null && !alumnoOld.equals(alumnoNew)) {
                alumnoOld.getHistorialList().remove(historial);
                alumnoOld = em.merge(alumnoOld);
            }
            if (alumnoNew != null && !alumnoNew.equals(alumnoOld)) {
                alumnoNew.getHistorialList().add(historial);
                alumnoNew = em.merge(alumnoNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                HistorialPK id = historial.getHistorialPK();
                if (findHistorial(id) == null) {
                    throw new NonexistentEntityException("The historial with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(HistorialPK id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Historial historial;
            try {
                historial = em.getReference(Historial.class, id);
                historial.getHistorialPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The historial with id " + id + " no longer exists.", enfe);
            }
            Alumno alumno = historial.getAlumno();
            if (alumno != null) {
                alumno.getHistorialList().remove(historial);
                alumno = em.merge(alumno);
            }
            em.remove(historial);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Historial> findHistorialEntities() {
        return findHistorialEntities(true, -1, -1);
    }

    public List<Historial> findHistorialEntities(int maxResults, int firstResult) {
        return findHistorialEntities(false, maxResults, firstResult);
    }

    private List<Historial> findHistorialEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Historial.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Historial findHistorial(HistorialPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Historial.class, id);
        } finally {
            em.close();
        }
    }

    public int getHistorialCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Historial> rt = cq.from(Historial.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
