/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anuar
 */
@Embeddable
public class ImagenPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "idvideojuego")
    private int idvideojuego;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idimagen")
    private int idimagen;

    public ImagenPK() {
    }

    public ImagenPK(int idvideojuego, int idimagen) {
        this.idvideojuego = idvideojuego;
        this.idimagen = idimagen;
    }

    public int getIdvideojuego() {
        return idvideojuego;
    }

    public void setIdvideojuego(int idvideojuego) {
        this.idvideojuego = idvideojuego;
    }

    public int getIdimagen() {
        return idimagen;
    }

    public void setIdimagen(int idimagen) {
        this.idimagen = idimagen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idvideojuego;
        hash += (int) idimagen;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImagenPK)) {
            return false;
        }
        ImagenPK other = (ImagenPK) object;
        if (this.idvideojuego != other.idvideojuego) {
            return false;
        }
        if (this.idimagen != other.idimagen) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ImagenPK[ idvideojuego=" + idvideojuego + ", idimagen=" + idimagen + " ]";
    }
    
}
