/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Lob;

/**
 *
 * @author anuar
 */
@Embeddable
public class HistorialPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "correo")
    private String correo;
    @Basic(optional = false)
    @Column(name = "semestre")
    private int semestre;
    @Basic(optional = false)
    @Lob
    @Column(name = "archivo")
    private byte[] archivo;

    public HistorialPK() {
    }

    public HistorialPK(String correo, int semestre, byte[] archivo) {
        this.correo = correo;
        this.semestre = semestre;
        this.archivo = archivo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (correo != null ? correo.hashCode() : 0);
        hash += (int) semestre;
        hash += (archivo != null ? archivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialPK)) {
            return false;
        }
        HistorialPK other = (HistorialPK) object;
        if ((this.correo == null && other.correo != null) || (this.correo != null && !this.correo.equals(other.correo))) {
            return false;
        }
        if (this.semestre != other.semestre) {
            return false;
        }
        if ((this.archivo == null && other.archivo != null) || (this.archivo != null && !this.archivo.equals(other.archivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.HistorialPK[ correo=" + correo + ", semestre=" + semestre + ", archivo=" + archivo + " ]";
    }
    
}
