/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
/**
 *
 * @author anuar
 */
@Embeddable
public class VideoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "idvideojuego")
    private int idvideojuego;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idvideo")
    private int idvideo;

    public VideoPK() {
    }

    public VideoPK(int idvideojuego, int idvideo) {
        this.idvideojuego = idvideojuego;
        this.idvideo = idvideo;
    }

    public int getIdvideojuego() {
        return idvideojuego;
    }

    public void setIdvideojuego(int idvideojuego) {
        this.idvideojuego = idvideojuego;
    }

    public int getIdvideo() {
        return idvideo;
    }

    public void setIdvideo(int idvideo) {
        this.idvideo = idvideo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idvideojuego;
        hash += (int) idvideo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VideoPK)) {
            return false;
        }
        VideoPK other = (VideoPK) object;
        if (this.idvideojuego != other.idvideojuego) {
            return false;
        }
        if (this.idvideo != other.idvideo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.VideoPK[ idvideojuego=" + idvideojuego + ", idvideo=" + idvideo + " ]";
    }
    
}
