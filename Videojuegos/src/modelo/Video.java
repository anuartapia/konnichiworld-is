/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anuar
 */
@Entity
@Table(name = "video")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Video.findAll", query = "SELECT v FROM Video v"),
    @NamedQuery(name = "Video.findByIdvideojuego", query = "SELECT v FROM Video v WHERE v.videoPK.idvideojuego = :idvideojuego"),
    @NamedQuery(name = "Video.findByIdvideo", query = "SELECT v FROM Video v WHERE v.videoPK.idvideo = :idvideo"),
    @NamedQuery(name = "Video.findByRutaarchivo", query = "SELECT v FROM Video v WHERE v.rutaarchivo = :rutaarchivo")})
public class Video implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VideoPK videoPK;
    @Size(max = 500)
    @Column(name = "rutaarchivo")
    private String rutaarchivo;
    @JoinColumn(name = "idvideojuego", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Videojuego videojuego;

    public Video() {
    }

    public Video(VideoPK videoPK) {
        this.videoPK = videoPK;
    }

    public Video(int idvideojuego, int idvideo) {
        this.videoPK = new VideoPK(idvideojuego, idvideo);
    }

    public VideoPK getVideoPK() {
        return videoPK;
    }

    public void setVideoPK(VideoPK videoPK) {
        this.videoPK = videoPK;
    }

    public String getRutaarchivo() {
        return rutaarchivo;
    }

    public void setRutaarchivo(String rutaarchivo) {
        this.rutaarchivo = rutaarchivo;
    }

    public Videojuego getVideojuego() {
        return videojuego;
    }

    public void setVideojuego(Videojuego videojuego) {
        this.videojuego = videojuego;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (videoPK != null ? videoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Video)) {
            return false;
        }
        Video other = (Video) object;
        if ((this.videoPK == null && other.videoPK != null) || (this.videoPK != null && !this.videoPK.equals(other.videoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Video[ videoPK=" + videoPK + " ]";
    }
    
}
