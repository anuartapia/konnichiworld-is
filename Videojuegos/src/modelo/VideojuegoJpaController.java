/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import modelo.exceptions.IllegalOrphanException;
import modelo.exceptions.NonexistentEntityException;
import modelo.exceptions.PreexistingEntityException;
import modelo.exceptions.RollbackFailureException;

/**
 *
 * @author anuar
 */
public class VideojuegoJpaController implements Serializable {

    public VideojuegoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Videojuego videojuego) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (videojuego.getDesarrolladorList() == null) {
            videojuego.setDesarrolladorList(new ArrayList<Desarrollador>());
        }
        if (videojuego.getAlumnoList() == null) {
            videojuego.setAlumnoList(new ArrayList<Alumno>());
        }
        if (videojuego.getImagenList() == null) {
            videojuego.setImagenList(new ArrayList<Imagen>());
        }
        if (videojuego.getVideoList() == null) {
            videojuego.setVideoList(new ArrayList<Video>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Desarrollador> attachedDesarrolladorList = new ArrayList<Desarrollador>();
            for (Desarrollador desarrolladorListDesarrolladorToAttach : videojuego.getDesarrolladorList()) {
                desarrolladorListDesarrolladorToAttach = em.getReference(desarrolladorListDesarrolladorToAttach.getClass(), desarrolladorListDesarrolladorToAttach.getUsuario());
                attachedDesarrolladorList.add(desarrolladorListDesarrolladorToAttach);
            }
            videojuego.setDesarrolladorList(attachedDesarrolladorList);
            List<Alumno> attachedAlumnoList = new ArrayList<Alumno>();
            for (Alumno alumnoListAlumnoToAttach : videojuego.getAlumnoList()) {
                alumnoListAlumnoToAttach = em.getReference(alumnoListAlumnoToAttach.getClass(), alumnoListAlumnoToAttach.getCorreo());
                attachedAlumnoList.add(alumnoListAlumnoToAttach);
            }
            videojuego.setAlumnoList(attachedAlumnoList);
            List<Imagen> attachedImagenList = new ArrayList<Imagen>();
            for (Imagen imagenListImagenToAttach : videojuego.getImagenList()) {
                imagenListImagenToAttach = em.getReference(imagenListImagenToAttach.getClass(), imagenListImagenToAttach.getImagenPK());
                attachedImagenList.add(imagenListImagenToAttach);
            }
            videojuego.setImagenList(attachedImagenList);
            List<Video> attachedVideoList = new ArrayList<Video>();
            for (Video videoListVideoToAttach : videojuego.getVideoList()) {
                videoListVideoToAttach = em.getReference(videoListVideoToAttach.getClass(), videoListVideoToAttach.getVideoPK());
                attachedVideoList.add(videoListVideoToAttach);
            }
            videojuego.setVideoList(attachedVideoList);
            em.persist(videojuego);
            for (Desarrollador desarrolladorListDesarrollador : videojuego.getDesarrolladorList()) {
                desarrolladorListDesarrollador.getVideojuegoList().add(videojuego);
                desarrolladorListDesarrollador = em.merge(desarrolladorListDesarrollador);
            }
            for (Alumno alumnoListAlumno : videojuego.getAlumnoList()) {
                alumnoListAlumno.getVideojuegoList().add(videojuego);
                alumnoListAlumno = em.merge(alumnoListAlumno);
            }
            for (Imagen imagenListImagen : videojuego.getImagenList()) {
                Videojuego oldVideojuegoOfImagenListImagen = imagenListImagen.getVideojuego();
                imagenListImagen.setVideojuego(videojuego);
                imagenListImagen = em.merge(imagenListImagen);
                if (oldVideojuegoOfImagenListImagen != null) {
                    oldVideojuegoOfImagenListImagen.getImagenList().remove(imagenListImagen);
                    oldVideojuegoOfImagenListImagen = em.merge(oldVideojuegoOfImagenListImagen);
                }
            }
            for (Video videoListVideo : videojuego.getVideoList()) {
                Videojuego oldVideojuegoOfVideoListVideo = videoListVideo.getVideojuego();
                videoListVideo.setVideojuego(videojuego);
                videoListVideo = em.merge(videoListVideo);
                if (oldVideojuegoOfVideoListVideo != null) {
                    oldVideojuegoOfVideoListVideo.getVideoList().remove(videoListVideo);
                    oldVideojuegoOfVideoListVideo = em.merge(oldVideojuegoOfVideoListVideo);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findVideojuego(videojuego.getId()) != null) {
                throw new PreexistingEntityException("Videojuego " + videojuego + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Videojuego videojuego) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Videojuego persistentVideojuego = em.find(Videojuego.class, videojuego.getId());
            List<Desarrollador> desarrolladorListOld = persistentVideojuego.getDesarrolladorList();
            List<Desarrollador> desarrolladorListNew = videojuego.getDesarrolladorList();
            List<Alumno> alumnoListOld = persistentVideojuego.getAlumnoList();
            List<Alumno> alumnoListNew = videojuego.getAlumnoList();
            List<Imagen> imagenListOld = persistentVideojuego.getImagenList();
            List<Imagen> imagenListNew = videojuego.getImagenList();
            List<Video> videoListOld = persistentVideojuego.getVideoList();
            List<Video> videoListNew = videojuego.getVideoList();
            List<String> illegalOrphanMessages = null;
            for (Imagen imagenListOldImagen : imagenListOld) {
                if (!imagenListNew.contains(imagenListOldImagen)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Imagen " + imagenListOldImagen + " since its videojuego field is not nullable.");
                }
            }
            for (Video videoListOldVideo : videoListOld) {
                if (!videoListNew.contains(videoListOldVideo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Video " + videoListOldVideo + " since its videojuego field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Desarrollador> attachedDesarrolladorListNew = new ArrayList<Desarrollador>();
            for (Desarrollador desarrolladorListNewDesarrolladorToAttach : desarrolladorListNew) {
                desarrolladorListNewDesarrolladorToAttach = em.getReference(desarrolladorListNewDesarrolladorToAttach.getClass(), desarrolladorListNewDesarrolladorToAttach.getUsuario());
                attachedDesarrolladorListNew.add(desarrolladorListNewDesarrolladorToAttach);
            }
            desarrolladorListNew = attachedDesarrolladorListNew;
            videojuego.setDesarrolladorList(desarrolladorListNew);
            List<Alumno> attachedAlumnoListNew = new ArrayList<Alumno>();
            for (Alumno alumnoListNewAlumnoToAttach : alumnoListNew) {
                alumnoListNewAlumnoToAttach = em.getReference(alumnoListNewAlumnoToAttach.getClass(), alumnoListNewAlumnoToAttach.getCorreo());
                attachedAlumnoListNew.add(alumnoListNewAlumnoToAttach);
            }
            alumnoListNew = attachedAlumnoListNew;
            videojuego.setAlumnoList(alumnoListNew);
            List<Imagen> attachedImagenListNew = new ArrayList<Imagen>();
            for (Imagen imagenListNewImagenToAttach : imagenListNew) {
                imagenListNewImagenToAttach = em.getReference(imagenListNewImagenToAttach.getClass(), imagenListNewImagenToAttach.getImagenPK());
                attachedImagenListNew.add(imagenListNewImagenToAttach);
            }
            imagenListNew = attachedImagenListNew;
            videojuego.setImagenList(imagenListNew);
            List<Video> attachedVideoListNew = new ArrayList<Video>();
            for (Video videoListNewVideoToAttach : videoListNew) {
                videoListNewVideoToAttach = em.getReference(videoListNewVideoToAttach.getClass(), videoListNewVideoToAttach.getVideoPK());
                attachedVideoListNew.add(videoListNewVideoToAttach);
            }
            videoListNew = attachedVideoListNew;
            videojuego.setVideoList(videoListNew);
            videojuego = em.merge(videojuego);
            for (Desarrollador desarrolladorListOldDesarrollador : desarrolladorListOld) {
                if (!desarrolladorListNew.contains(desarrolladorListOldDesarrollador)) {
                    desarrolladorListOldDesarrollador.getVideojuegoList().remove(videojuego);
                    desarrolladorListOldDesarrollador = em.merge(desarrolladorListOldDesarrollador);
                }
            }
            for (Desarrollador desarrolladorListNewDesarrollador : desarrolladorListNew) {
                if (!desarrolladorListOld.contains(desarrolladorListNewDesarrollador)) {
                    desarrolladorListNewDesarrollador.getVideojuegoList().add(videojuego);
                    desarrolladorListNewDesarrollador = em.merge(desarrolladorListNewDesarrollador);
                }
            }
            for (Alumno alumnoListOldAlumno : alumnoListOld) {
                if (!alumnoListNew.contains(alumnoListOldAlumno)) {
                    alumnoListOldAlumno.getVideojuegoList().remove(videojuego);
                    alumnoListOldAlumno = em.merge(alumnoListOldAlumno);
                }
            }
            for (Alumno alumnoListNewAlumno : alumnoListNew) {
                if (!alumnoListOld.contains(alumnoListNewAlumno)) {
                    alumnoListNewAlumno.getVideojuegoList().add(videojuego);
                    alumnoListNewAlumno = em.merge(alumnoListNewAlumno);
                }
            }
            for (Imagen imagenListNewImagen : imagenListNew) {
                if (!imagenListOld.contains(imagenListNewImagen)) {
                    Videojuego oldVideojuegoOfImagenListNewImagen = imagenListNewImagen.getVideojuego();
                    imagenListNewImagen.setVideojuego(videojuego);
                    imagenListNewImagen = em.merge(imagenListNewImagen);
                    if (oldVideojuegoOfImagenListNewImagen != null && !oldVideojuegoOfImagenListNewImagen.equals(videojuego)) {
                        oldVideojuegoOfImagenListNewImagen.getImagenList().remove(imagenListNewImagen);
                        oldVideojuegoOfImagenListNewImagen = em.merge(oldVideojuegoOfImagenListNewImagen);
                    }
                }
            }
            for (Video videoListNewVideo : videoListNew) {
                if (!videoListOld.contains(videoListNewVideo)) {
                    Videojuego oldVideojuegoOfVideoListNewVideo = videoListNewVideo.getVideojuego();
                    videoListNewVideo.setVideojuego(videojuego);
                    videoListNewVideo = em.merge(videoListNewVideo);
                    if (oldVideojuegoOfVideoListNewVideo != null && !oldVideojuegoOfVideoListNewVideo.equals(videojuego)) {
                        oldVideojuegoOfVideoListNewVideo.getVideoList().remove(videoListNewVideo);
                        oldVideojuegoOfVideoListNewVideo = em.merge(oldVideojuegoOfVideoListNewVideo);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = videojuego.getId();
                if (findVideojuego(id) == null) {
                    throw new NonexistentEntityException("The videojuego with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Videojuego videojuego;
            try {
                videojuego = em.getReference(Videojuego.class, id);
                videojuego.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The videojuego with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Imagen> imagenListOrphanCheck = videojuego.getImagenList();
            for (Imagen imagenListOrphanCheckImagen : imagenListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Videojuego (" + videojuego + ") cannot be destroyed since the Imagen " + imagenListOrphanCheckImagen + " in its imagenList field has a non-nullable videojuego field.");
            }
            List<Video> videoListOrphanCheck = videojuego.getVideoList();
            for (Video videoListOrphanCheckVideo : videoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Videojuego (" + videojuego + ") cannot be destroyed since the Video " + videoListOrphanCheckVideo + " in its videoList field has a non-nullable videojuego field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Desarrollador> desarrolladorList = videojuego.getDesarrolladorList();
            for (Desarrollador desarrolladorListDesarrollador : desarrolladorList) {
                desarrolladorListDesarrollador.getVideojuegoList().remove(videojuego);
                desarrolladorListDesarrollador = em.merge(desarrolladorListDesarrollador);
            }
            List<Alumno> alumnoList = videojuego.getAlumnoList();
            for (Alumno alumnoListAlumno : alumnoList) {
                alumnoListAlumno.getVideojuegoList().remove(videojuego);
                alumnoListAlumno = em.merge(alumnoListAlumno);
            }
            em.remove(videojuego);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Videojuego> findVideojuegoEntities() {
        return findVideojuegoEntities(true, -1, -1);
    }

    public List<Videojuego> findVideojuegoEntities(int maxResults, int firstResult) {
        return findVideojuegoEntities(false, maxResults, firstResult);
    }

    private List<Videojuego> findVideojuegoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Videojuego.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Videojuego findVideojuego(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Videojuego.class, id);
        } finally {
            em.close();
        }
    }

    public int getVideojuegoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Videojuego> rt = cq.from(Videojuego.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
