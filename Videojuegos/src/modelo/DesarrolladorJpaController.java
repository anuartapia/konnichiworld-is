/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import modelo.exceptions.NonexistentEntityException;
import modelo.exceptions.PreexistingEntityException;
import modelo.exceptions.RollbackFailureException;

/**
 *
 * @author anuar
 */
public class DesarrolladorJpaController implements Serializable {

    public DesarrolladorJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Desarrollador desarrollador) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (desarrollador.getVideojuegoList() == null) {
            desarrollador.setVideojuegoList(new ArrayList<Videojuego>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Videojuego> attachedVideojuegoList = new ArrayList<Videojuego>();
            for (Videojuego videojuegoListVideojuegoToAttach : desarrollador.getVideojuegoList()) {
                videojuegoListVideojuegoToAttach = em.getReference(videojuegoListVideojuegoToAttach.getClass(), videojuegoListVideojuegoToAttach.getId());
                attachedVideojuegoList.add(videojuegoListVideojuegoToAttach);
            }
            desarrollador.setVideojuegoList(attachedVideojuegoList);
            em.persist(desarrollador);
            for (Videojuego videojuegoListVideojuego : desarrollador.getVideojuegoList()) {
                videojuegoListVideojuego.getDesarrolladorList().add(desarrollador);
                videojuegoListVideojuego = em.merge(videojuegoListVideojuego);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findDesarrollador(desarrollador.getUsuario()) != null) {
                throw new PreexistingEntityException("Desarrollador " + desarrollador + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Desarrollador desarrollador) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Desarrollador persistentDesarrollador = em.find(Desarrollador.class, desarrollador.getUsuario());
            List<Videojuego> videojuegoListOld = persistentDesarrollador.getVideojuegoList();
            List<Videojuego> videojuegoListNew = desarrollador.getVideojuegoList();
            List<Videojuego> attachedVideojuegoListNew = new ArrayList<Videojuego>();
            for (Videojuego videojuegoListNewVideojuegoToAttach : videojuegoListNew) {
                videojuegoListNewVideojuegoToAttach = em.getReference(videojuegoListNewVideojuegoToAttach.getClass(), videojuegoListNewVideojuegoToAttach.getId());
                attachedVideojuegoListNew.add(videojuegoListNewVideojuegoToAttach);
            }
            videojuegoListNew = attachedVideojuegoListNew;
            desarrollador.setVideojuegoList(videojuegoListNew);
            desarrollador = em.merge(desarrollador);
            for (Videojuego videojuegoListOldVideojuego : videojuegoListOld) {
                if (!videojuegoListNew.contains(videojuegoListOldVideojuego)) {
                    videojuegoListOldVideojuego.getDesarrolladorList().remove(desarrollador);
                    videojuegoListOldVideojuego = em.merge(videojuegoListOldVideojuego);
                }
            }
            for (Videojuego videojuegoListNewVideojuego : videojuegoListNew) {
                if (!videojuegoListOld.contains(videojuegoListNewVideojuego)) {
                    videojuegoListNewVideojuego.getDesarrolladorList().add(desarrollador);
                    videojuegoListNewVideojuego = em.merge(videojuegoListNewVideojuego);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = desarrollador.getUsuario();
                if (findDesarrollador(id) == null) {
                    throw new NonexistentEntityException("The desarrollador with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Desarrollador desarrollador;
            try {
                desarrollador = em.getReference(Desarrollador.class, id);
                desarrollador.getUsuario();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The desarrollador with id " + id + " no longer exists.", enfe);
            }
            List<Videojuego> videojuegoList = desarrollador.getVideojuegoList();
            for (Videojuego videojuegoListVideojuego : videojuegoList) {
                videojuegoListVideojuego.getDesarrolladorList().remove(desarrollador);
                videojuegoListVideojuego = em.merge(videojuegoListVideojuego);
            }
            em.remove(desarrollador);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Desarrollador> findDesarrolladorEntities() {
        return findDesarrolladorEntities(true, -1, -1);
    }

    public List<Desarrollador> findDesarrolladorEntities(int maxResults, int firstResult) {
        return findDesarrolladorEntities(false, maxResults, firstResult);
    }

    private List<Desarrollador> findDesarrolladorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Desarrollador.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Desarrollador findDesarrollador(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Desarrollador.class, id);
        } finally {
            em.close();
        }
    }

    public int getDesarrolladorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Desarrollador> rt = cq.from(Desarrollador.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
