/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import modelo.exceptions.NonexistentEntityException;
import modelo.exceptions.PreexistingEntityException;
import modelo.exceptions.RollbackFailureException;

/**
 *
 * @author anuar
 */
public class VideoJpaController implements Serializable {

    public VideoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Video video) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (video.getVideoPK() == null) {
            video.setVideoPK(new VideoPK());
        }
        video.getVideoPK().setIdvideojuego(video.getVideojuego().getId());
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Videojuego videojuego = video.getVideojuego();
            if (videojuego != null) {
                videojuego = em.getReference(videojuego.getClass(), videojuego.getId());
                video.setVideojuego(videojuego);
            }
            em.persist(video);
            if (videojuego != null) {
                videojuego.getVideoList().add(video);
                videojuego = em.merge(videojuego);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findVideo(video.getVideoPK()) != null) {
                throw new PreexistingEntityException("Video " + video + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Video video) throws NonexistentEntityException, RollbackFailureException, Exception {
        video.getVideoPK().setIdvideojuego(video.getVideojuego().getId());
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Video persistentVideo = em.find(Video.class, video.getVideoPK());
            Videojuego videojuegoOld = persistentVideo.getVideojuego();
            Videojuego videojuegoNew = video.getVideojuego();
            if (videojuegoNew != null) {
                videojuegoNew = em.getReference(videojuegoNew.getClass(), videojuegoNew.getId());
                video.setVideojuego(videojuegoNew);
            }
            video = em.merge(video);
            if (videojuegoOld != null && !videojuegoOld.equals(videojuegoNew)) {
                videojuegoOld.getVideoList().remove(video);
                videojuegoOld = em.merge(videojuegoOld);
            }
            if (videojuegoNew != null && !videojuegoNew.equals(videojuegoOld)) {
                videojuegoNew.getVideoList().add(video);
                videojuegoNew = em.merge(videojuegoNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                VideoPK id = video.getVideoPK();
                if (findVideo(id) == null) {
                    throw new NonexistentEntityException("The video with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(VideoPK id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Video video;
            try {
                video = em.getReference(Video.class, id);
                video.getVideoPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The video with id " + id + " no longer exists.", enfe);
            }
            Videojuego videojuego = video.getVideojuego();
            if (videojuego != null) {
                videojuego.getVideoList().remove(video);
                videojuego = em.merge(videojuego);
            }
            em.remove(video);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Video> findVideoEntities() {
        return findVideoEntities(true, -1, -1);
    }

    public List<Video> findVideoEntities(int maxResults, int firstResult) {
        return findVideoEntities(false, maxResults, firstResult);
    }

    private List<Video> findVideoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Video.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Video findVideo(VideoPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Video.class, id);
        } finally {
            em.close();
        }
    }

    public int getVideoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Video> rt = cq.from(Video.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
