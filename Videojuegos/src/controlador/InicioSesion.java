package controlador;

import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import modelo.Alumno;
import modelo.AlumnoJpaController;
import modelo.Desarrollador;
import modelo.DesarrolladorJpaController;

/**
 * Controlador que se comunica con los modelos <code>AlumnoJpaController</code>
 * y <code>DesarrolladorJpaController</code> para verificar los datos de un
 * <code>Alumno</code> o de un <code>Desarrollador</code> en la base de datos.
 *
 * @author anuar
 */
public class InicioSesion {

    /**
     * Indica que los datos de un usuario son correctos.
     */
    public static final int OK = 0;

    /**
     * Indica que la contraseña de un usuario es incorrecta.
     */
    public static final int INCORRECTO = 1;

    /**
     * Indica que los datos de un usuario no existen.
     */
    public static final int INVALIDO = 2;

    /**
     * Controlador del modelo <code>Alumno</code>.
     */
    private final AlumnoJpaController ajc;

    /**
     * Controlador del modelo <code>Desarrollador</code>.
     */
    private final DesarrolladorJpaController djc;

    /**
     * Instancia que deberá contener los datos del <code>Alumno</code> obtenidos
     * a partir de su id y contraseña.
     */
    private Alumno alu;

    /**
     * Instancia que deberá contener los datos del <code>Desarrollador</code>
     * obtenidos a partir de su id y contraseña.
     */
    private Desarrollador des;

    /**
     * Contructor que sólo recibe el utx y el emf para poder crear los JPA
     *
     * @param utx UserTransaction
     * @param emf EntityManagerFactory
     */
    public InicioSesion(UserTransaction utx, EntityManagerFactory emf) {
        ajc = new AlumnoJpaController(utx, emf);
        djc = new DesarrolladorJpaController(utx, emf);
        alu = null;
        des = null;
    }

    /**
     * Regresa el <code>Alumno</code> cuyo id es el correo que le pasan. Regresa
     * null si no existe en la base de datos.
     *
     * @param correo
     * @return Alumno correspondiente al correo
     */
    public Alumno obtenAlumno(String correo) {
        return ajc.findAlumno(correo);
    }

    /**
     * Regresa el <code>Desarrollador</code> cuyo id es el correo que le pasan.
     * Regresa null si no existe en la base de datos.
     *
     * @param correo
     * @return Desarrollador correspondiente al correo
     */
    public Desarrollador obtenDesarrollador(String correo) {
        return djc.findDesarrollador(correo);
    }

    /**
     * Determina si los datos que le pasan pertenecen a un <code>Alumno</code> o
     * a un <code>Desarrollador</code> en la base de datos.
     *
     * @param correo
     * @param password
     * @return OK si son corectos los datos, INCORRECTO si no correponde la
     * contraseña e INVALIDO si no existe el usuario
     */
    public int verificaUsuario(String correo, String password) {
        alu = obtenAlumno(correo);//busca en los alumnos
        if (alu != null) {//encontró uno
            if (alu.getContrasena().equals(password)) {//password correcto
                return OK;
            } else {
                return INCORRECTO;
            }
        } else {//no encontró el alumno
            des = obtenDesarrollador(correo);//busca en los desarrolladores
            if (des != null) {//encontró uno
                if (des.getContrasena().equals(password)) {//password correcto
                    return OK;
                } else {
                    return INCORRECTO;
                }
            } else {//no econtró el desarrollador
                return INVALIDO;
            }
        }
    }

    /**
     * Indica si el correo que le pasan pertenece a un <code>Alumno</code>.
     *
     * @return true si el correo es de algun alumno en la base de datos
     */
    public boolean esAlumno(String correo) {
        return obtenAlumno(correo) != null;
    }
}
