/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import modelo.Alumno;
import modelo.AlumnoJpaController;
import modelo.exceptions.RollbackFailureException;
import controlador.ValidarDatos;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author esteban
 */
@WebServlet(name = "NuevoAlumno", urlPatterns = {"/NuevoAlumno"})
public class NuevoAlumno extends HttpServlet {

    @PersistenceUnit(unitName = "VideojuegosPU")
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction utx;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, RollbackFailureException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession(true);
            
            String correo = request.getParameter("correo");
            Pattern p = Pattern.compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
            Matcher m = p.matcher(correo);

            String nombre = request.getParameter("nombre");
            String ap = request.getParameter("apellidoPa");
            String am = request.getParameter("apellidoMa");

            String contra1 = request.getParameter("pwd1");
            String contra2 = request.getParameter("pwd2");

            //Semestre
            String semStr = request.getParameter("semestre");
            int semInt = parseInt(semStr);
            
            String diaS = request.getParameter("dia");
            String mesS = request.getParameter("mes");
            String anioS = request.getParameter("anio");
            //Objeto Fecha
            int dia = parseInt(diaS);
            int mes = parseInt(mesS);
            int anio = parseInt(anioS);

            Date fechaNac = new Date(anio, mes, dia);

            Alumno nuevo = new Alumno(correo);
            ValidarDatos v = new ValidarDatos();

            //Hay campos vacios
            if (nombre.isEmpty() || correo.isEmpty() || contra1.isEmpty() || contra2.isEmpty() || diaS.isEmpty() || mesS.isEmpty() || anioS.isEmpty() || semStr.isEmpty()) {
                session.setAttribute("error", "Hay campos vacios");

            } else {
                //No hay campos vacios, veo que la direccion de email sea válida
//                if (m.find()) {
//                    session.setAttribute("error", "La direccion de email no es correcta");
//
//                } else {
//                    //La direccion de email si es correcta, verifico que la contraseña tambien lo sea
                   if (v.isEmail(correo)) {
                        //Ahora verifico si la contraseña 1 y la contraseña 2 son iguales
                        if (contra1.equals(contra2)) {
                            try {
                                //Verifico que no se encuentre registrada la dirección de correo
                                AlumnoJpaController alumnoJpa = new AlumnoJpaController(utx, emf);
                                if (alumnoJpa.findAlumno(correo) == null) {

                                    //Legado a este punto significa que todo esta correcto, por lo tanto ingreso a la DB
                                    nuevo.setCorreo(correo);
                                    nuevo.setContrasena(contra1);
                                    nuevo.setNombre(nombre);
                                    nuevo.setAp(ap);
                                    nuevo.setAm(am);
                                    nuevo.setFecha(fechaNac);
                                    nuevo.setSemestre(semInt);
                                    nuevo.setCredito(200);
                                    //Se conecta con el modelo y crea un nuevo registro en la base
                                    alumnoJpa.create(nuevo);
                                    //Guardo los datos en una sesion
                                    session.setAttribute("alumno", nuevo);
                                    response.sendRedirect("entrar.jsp");
                                    return;

                                } else {
                                    session.setAttribute("errorMessage", "Esta direccion de correo ya fue registrada");
                                    
                                }

                                return;

                            } catch (Exception e) {
                                out.println("Ocurrio la sig exception: " + e);
                            }

                        } else {
                            session.setAttribute("errorMessage", "Las contraseñas no son iguales");

                        }

                    //} else {
                  //      session.setAttribute("error", "Contraseña no es válida");

                   // }

                }
            }

            // get back to order.jsp page using forward
            session.setAttribute("alumno", nuevo);
            response.sendRedirect("registro.jsp");

        }

//            AlumnoJpaController alumnoJpa = new AlumnoJpaController(utx, emf);
//            alumnoJpa.create(nuevo);
//
//
//            session.setAttribute("alumno", nuevo);
//            String c = ((Alumno) session.getAttribute("alumno")).getCorreo();
//            response.sendRedirect("index.jsp");
//            return;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(NuevoAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(NuevoAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
