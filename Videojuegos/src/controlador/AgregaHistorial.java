/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import modelo.Alumno;
import modelo.AlumnoJpaController;
import modelo.Historial;
import modelo.HistorialJpaController;
import modelo.exceptions.RollbackFailureException;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author tania
 */
public class AgregaHistorial extends HttpServlet {
@PersistenceUnit(unitName="VideojuegosPU")
    private EntityManagerFactory emf;
    
    @Resource
    private UserTransaction utx;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, RollbackFailureException, Exception {
        
            PrintWriter out = response.getWriter();

            response.setContentType("text/html;charset=UTF-8");
            emf = Persistence.createEntityManagerFactory("VideojuegosPU");
            HttpSession session = request.getSession();
            
            
            String correo=((Alumno)session.getAttribute("alumno")).getCorreo();
           
//            int semestre=Integer.parseInt(request.getParameter("semestre"));
            
            HistorialJpaController historialJpa = new HistorialJpaController(utx, emf);
            AlumnoJpaController alumnoJpa = new AlumnoJpaController(utx, emf);
            Alumno a = alumnoJpa.findAlumno(correo);
            
            
if (ServletFileUpload.isMultipartContent(request)) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(Long.MAX_VALUE);

            List listUploadFiles = null;
            FileItem item = null;
            
            try {
                listUploadFiles = upload.parseRequest(request);

                Iterator it = listUploadFiles.iterator();
                while (it.hasNext()) {
                    item = (FileItem) it.next();
                    if (!item.isFormField()) {
                        if (item.getSize() > 0) {
                            Historial historialNuevo=new Historial(correo,3,item.get());
                            historialJpa.create(historialNuevo);
                            out.println("alert(\"Listo\")");
                            
                        }
                    }
                }
            }catch (FileUploadException e) {
                e.printStackTrace();
            } catch (Exception e) {
                 e.printStackTrace();
            }
            //response.sendRedirect("cuenta.jsp");

            //session.setAttribute("alumno", a);
    }
    
        response.sendRedirect("cuenta.jsp");
            session.setAttribute("alumno", a);
    
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    try {
        processRequest(request, response);
    } catch (Exception ex) {
        Logger.getLogger(AgregaHistorial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    try {
        processRequest(request, response);
    } catch (Exception ex) {
        Logger.getLogger(AgregaHistorial.class.getName()).log(Level.SEVERE, null, ex);
    }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}