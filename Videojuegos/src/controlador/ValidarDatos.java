/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author esteban
 */
public class ValidarDatos {
    /**
     * Método que verifica el campo del correo
     *
     * @param correo cadena la cual se verificará que cumpla con los parametros
     * de un correo
     * @return True si el correo es valido; false en otro caso.
     */
    public boolean isEmail(String correo) {
        Pattern pat = null;
        Matcher mat = null;
        pat = Pattern.compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
        mat = pat.matcher(correo);
        if (mat.find()) {
            System.out.println("[" + mat.group() + "]");
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Método que recibe un arreglo de objetos y valida si estan en blanco.
     *
     * @param datos Arreglo a validar.
     * @return
     */
    public boolean validar(Object[] datos) {
        boolean flag = false;
        for (int i = 0; i <= datos.length - 1; i++) {
            flag = !datos[i].toString().isEmpty(); //Si alguno de los datos es NO_VACIO, regresa TRUE
        }
        return flag;
    }
    
    /**
     * Método crea un arreglo de datos y verifica si estan vacios.
     *
     * @return True
     */
    public boolean validaDatos() {
        Object[] datos = {};//Este es el arreglo de datos
        boolean flag = false;
        //Si hay campos en blanco
        if (validar(datos)) {
            //No hay campos en blanco en el formulario.
            flag = true;
        } else {
            //Hay campos en blanco en el formulario.
        }
        return flag;
    }
    
    
    public boolean isUsernameOrPasswordValid(String $cadena) {
        char[] cadena = $cadena.toLowerCase().toCharArray();
 
        //Compruebo la longitud
        if (cadena.length <= 6) {
            return false;
        }
        for (int i = 0; i < cadena.length; i++) {
            //Compruebo que no existan caracteres especiales (solamento los que podrian ser usados para una inyeccion SQL o perjudicar en la consulta);
            if (cadena[i] == ' '
                    || cadena[i] == '='
                    || cadena[i] == '?'
                    || cadena[i] == '+'
                    || cadena[i] == '*'
                    || cadena[i] == '-'
                    || cadena[i] == '%'
                    || cadena[i] == '/'
                    || cadena[i] == '.'
                    || cadena[i] == ','
                    || cadena[i] == ';'
                    || cadena[i] == '!'
                    || cadena[i] == '<'
                    || cadena[i] == '>'
                    || cadena[i] == ':') {
                return false;
            }
 
        }
        return true;
    }
}
