/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.util.Date;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import modelo.Alumno;
import modelo.AlumnoJpaController;
import modelo.exceptions.RollbackFailureException;

/**
 *Servlet que maneja las peticiones de <code>Administrar AlumnoIH.jsp</code>
 * @author tania
 */
@WebServlet(name = "CuentaControlador", urlPatterns = {"/CuentaControlador"})
public class CuentaControlador extends HttpServlet {
    
    @PersistenceUnit(unitName="VideojuegosPU")
    private EntityManagerFactory emf;
    
    @Resource
    private UserTransaction utx;
    
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, RollbackFailureException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            emf = Persistence.createEntityManagerFactory("VideojuegosPU");
            HttpSession session = request.getSession();
            
            //Obtenemos el correo del <code>Alumno</code> da la sesión actual
            String c=((Alumno)session.getAttribute("alumno")).getCorreo();
            
            //Obtenemos el contenido del los parámetro del formulario que se encuetra en <code>Administrar AlumnoIH.jsp</code>
            String nombre=request.getParameter("Nombre");
            String ap=request.getParameter("Ap");
            String am=request.getParameter("Am");
            int dia = Integer.parseInt(request.getParameter("dia"));
            int mes = Integer.parseInt(request.getParameter("mes"));
            String anio =request.getParameter("elanio");
            //String correoN=request.getParameter("correo");
            
            //Creamos un <code>Date</code> para la nueva fecha
            Date fechaNac=new Date(parseInt(anio),mes-1,dia);
            
            
            AlumnoJpaController alumnoJpa = new AlumnoJpaController(utx, emf);
            
            //Busca al <code>Alumno</code> en la base de datos
            Alumno a = alumnoJpa.findAlumno(c);
            
            /*actualiza los datos del <code>Alumno</code> con el <code>AlumnoJpaController</code>*/
            a.setNombre(nombre);
            a.setAp(ap);
            a.setAm(am);
            a.setFecha(fechaNac);
            alumnoJpa.edit(a);
                
            response.sendRedirect("cuenta.jsp");//redirije al jsp cuenta
            session.setAttribute("alumno", a);//le pasa la sesión 
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CuentaControlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CuentaControlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}