/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Videojuego;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;
import modelo.Desarrollador;
import modelo.Imagen;
import modelo.ImagenJpaController;
import modelo.ImagenPK;
import modelo.Video;
import modelo.VideoJpaController;
import modelo.VideoPK;
import modelo.VideojuegoJpaController;
import modelo.exceptions.NonexistentEntityException;
import modelo.exceptions.RollbackFailureException;

/**
 *
 * @author jose
 */
@WebServlet(name = "Administrador", urlPatterns = {"/Administrador"})
public class AdministradorDesarrolador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final char OK = '1';
    private static final char ERROR = '0';
    byte[] buffer = null;
    EntityManager em;
    VideojuegoJpaController controlJuego;
    ImagenJpaController controlImagen;
    VideoJpaController controlVideo;
    List<Videojuego> juegos;
    List<Imagen> imagenes;
    List<Video> videos;
    HttpSession session;
    

    @PersistenceUnit(unitName = "VideojuegosPU")
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction utx;
    /**
    Funcion que se encarga de verificar la sesion si es correcta y si no rederigir al index
    */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Desarrollador desa = (Desarrollador) session.getAttribute("desarrollador");
            if(desa.getCorreo().equals("")||desa.getCorreo()==null)
                response.sendRedirect("index.jsp");
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet administrador</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet administrador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Metodo que se encarga de procesar las peticiones Ajax
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("operacion").equalsIgnoreCase("buffer")) {
            int tam = Integer.parseInt(request.getParameter("size"));
            buffer = new byte[tam];
            response.setContentType("text/plain");
            response.getWriter().write(OK);
        }
        //processRequest(request, response);
    }

    /**
     * 
     * Metodo que se encarga de procesar las peticiones Ajax
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

        Gson gson = new Gson();
        Videojuego juego;
        String palabra;
        int inicio;
        int fin;
        char res;
        ArrayList datos;
        String json;

        switch (request.getParameter("operacion")) {

            case "listar":

                List<Videojuego> juegos = busqueda(request.getParameter("letra"));
                datos = new ArrayList();
                for (int i = 0; i < juegos.size(); i++) {
                    datos.add(juegos.get(i));
                }
                json = gson.toJson(datos);

                response.setContentType("text/plain");
                response.setCharacterEncoding("utf-8");
                response.getWriter().write(json);

                break;


            case "creaJuego":

                palabra = request.getParameter("bits");
                inicio = Integer.parseInt(request.getParameter("i"));
                fin = Integer.parseInt(request.getParameter("f"));

                llenaBuffer(palabra, inicio, fin);
                response.setContentType("text/plain");
                response.setCharacterEncoding("utf-8");
                response.getWriter().write(OK);

                break;

            case "guardaJuego":

                response.setContentType("text/plain");
                response.setCharacterEncoding("utf-8");
                datos = new ArrayList();
                juego = new Videojuego();

                if (!esNumero(request.getParameter("costo"))) {

                    datos.add("El costo debe de ser un numero");
                    json = gson.toJson(datos);
                    response.getWriter().write(json);

                    return;
                }

                juego.setId(buscaId());

                if (nombreExiste(request.getParameter("nombre"))) {
                    datos.add("El nombre del Videojuego ya existe");
                    json = gson.toJson(datos);
                    response.getWriter().write(json);
                    return;
                }

                juego.setNombre(request.getParameter("nombre"));
                juego.setDesarrollador(request.getParameter("desarrollador"));
                juego.setCosto(Integer.parseInt(request.getParameter("costo")));
                juego.setDescripcion(request.getParameter("descripcion"));
                Date tem = new Date(request.getParameter("fecha"));
                juego.setFecha(tem);

                juego.setEjecutable(buffer);

                if (guardaJuego(juego)) {

                    datos.add("1");
                    datos.add(juego.getId());
                    json = gson.toJson(datos);
                    response.getWriter().write(json);
                } else {
                    datos.add("No Se pudo guardar");
                    datos.add(juego.getId());
                    json = gson.toJson(datos);
                    response.getWriter().write(json);
                }
                break;

            case "creaImagen":

                response.setContentType("text/plain");
                response.setCharacterEncoding("utf-8");

                int id = Integer.parseInt(request.getParameter("id"));

                palabra = request.getParameter("imagenes");

                ArrayList<String> rutas = convierte(palabra,0);

                Imagen img1;

                for (int i = 0; i < rutas.size(); i++) {
                    img1 = new Imagen(id, i + 1);
                    img1.setRutaarchivo(rutas.get(i));
                    img1.setVideojuego(recuperaJuego(id));
                    // persist(img1);
                    if(!guardaImagen(img1)){
                    response.getWriter().write("Error al guardar Imagen");
                    return;
                    }
                }

                response.getWriter().write(OK + "");
                break;

            case "creaVideo":

                response.setContentType("text/plain");
                response.setCharacterEncoding("utf-8");

                id = Integer.parseInt(request.getParameter("id"));

                palabra = request.getParameter("videos");

                rutas = convierte(palabra,1);

                Video vid1;

                for (int i = 0; i < rutas.size(); i++) {
                    vid1 = new Video(id, i + 1);
                    vid1.setRutaarchivo(rutas.get(i));
                    vid1.setVideojuego(recuperaJuego(id));
                    //persist(vid1);
                    if(!guardaVideo(vid1)){
                    response.getWriter().write("Error al guardar video");
                    return;
                    }
                        
                }

                response.getWriter().write(OK + "");

                break;

            case "buscaJuego":

                response.setContentType("text/plain");
                response.setCharacterEncoding("utf-8");

                id = Integer.parseInt(request.getParameter("id"));
                juego = recuperaJuego(id);
                if (juego != null) {
                    datos = new ArrayList();
                    juego.setEjecutable(null);
                    
                    datos = clone(juego.getImagenList(),juego.getVideoList());
                    juego.setImagenList(null);
                    juego.setVideoList(null);
                    juego.setAlumnoList(null);
                    //juego.setSubirList(null);
                    //juego.setBajarList(null);
                    datos.add(0,juego);
                    json = gson.toJson(datos);
                    response.getWriter().write(json);
                } else {
                    response.getWriter().write(ERROR);
                }

                break;

            case "actualizaJuego":

                juego = new Videojuego();
                datos = new ArrayList();
                if (!esNumero(request.getParameter("costo"))) {
                    datos.add("El costo debe de ser un numero");
                    json = gson.toJson(datos);
                    response.getWriter().write(json);
                    return;
                }
                
                id = Integer.parseInt(request.getParameter("id"));
                juego.setId(id);

                juego.setNombre(request.getParameter("nombre"));
                juego.setDesarrollador(request.getParameter("desarrollador"));
                juego.setCosto(Integer.parseInt(request.getParameter("costo")));
                juego.setDescripcion(request.getParameter("descripcion"));
                tem = new Date(request.getParameter("fecha"));
                juego.setFecha(tem);
                if(actualiza(juego,request.getParameter("imagenes"),request.getParameter("videos"))){
                    response.getWriter().write("Actualizacion Exitosa");
                    return;
                }else
                    response.getWriter().write("Error en Actualizacion");
                break;
         
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    /** Metodo para Saber si es numero
    * @param s - Cadena para saber si es numero
    * @return boolean si es numero o no
    */
    private boolean esNumero(String s) {
        try {
            Long.parseLong(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    /** Metodo para Cargar en buffer un juego en bytes
    * @param palabra - Juego en bytes
    * @param inicio - Donde empieza a copiarse
    * @param fin - Donde termina la cadena a copiar
    */
    
    private void llenaBuffer(String palabra, int inicio, int fin) {
        int j = 0;
        for (int i = inicio; i < fin; i++) {
            buffer[i] = (byte) palabra.charAt(j++);
        }
    }
    /** Metodo para Buscar juegos 
    * @param palabra - Inicio del nombre del videojuego
    * @return List - Lista que contiene los primeros 5 resultados 
    */
    private List busqueda(String palabra) {

        palabra = palabra.trim();
        
        palabra = palabra.toLowerCase();
        em = emf.createEntityManager();
        Query query = em.createNamedQuery("Videojuego.list");
        query.setParameter("letras", palabra + "%");
        query.setMaxResults(5);
        List videos = query.getResultList();

        System.out.print(" palabra " + palabra + "  " + videos.toString());

        return videos;
    }
    
    /** Metodo para Recuperar juego de la base de datos
    * @param id - Del videojuego a regresar
    * @return Videojuego - Videojuego encontrado null si no existe
    */
    
    private Videojuego recuperaJuego(int id) {

        controlJuego = new VideojuegoJpaController(utx, emf);

        try {
            Videojuego juego = controlJuego.findVideojuego(id);
            return juego;
        } catch (Exception ex) {
            Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    /** Metodo para Buscar Id disponible
    * @return Int - id disponible en la base de datos
    */
   
    private int buscaId() {
        em = emf.createEntityManager();
        Query query = em.createNamedQuery("Videojuego.takeId");
        List resultados = query.getResultList(); // Solo regresa un resultado
        if (resultados.isEmpty()) {
            return 1;
        } else {
            int id = (int) resultados.get(0);
            id++;
            return id;
        }
    }
    
    /** Metodo para saber si el nombre del juego existe
    * @param nombre - Del videojuego a verificar
    * @return Boolean - Existe true else en otro caso
    */
   
    private boolean nombreExiste(String nombre) {

        em = emf.createEntityManager();
        Query query = em.createNamedQuery("Videojuego.findByNombre");
        query.setParameter("nombre", nombre);
        List resultados = query.getResultList(); // Solo regresa un resultado
        if (resultados.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
   
    /** Metodo para Guardar un objeto en la base 
    * @param object - Objeto a guardar en la base
    */
    
    private static void persist(Object object) {
        EntityManagerFactory emf = javax.persistence.Persistence.createEntityManagerFactory("VideojuegosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
    
    /** Metodo para pasar de un arreglo byte a un String
    * @param arreglo - a convertir en String
    * @return String - arreglo bytes a String
    */
    
    private String convierteAstring(byte[] arreglo) {
        String palabra = "";

        for (int i = 0; i < arreglo.length; i++) {
            palabra += arreglo[i];
        }

        return palabra;
    }
    /** Metodo para procesar una lista de Imagen o video en un array 
     * cada elemento reprecenta un objeto
    * @param palabra - Cadena que contiene a los objetos
    * @param tipo - Saber que ruta se le asignara
    * @return ArrayList - Lista que contiene los objetos como rutas
    */
    
    private ArrayList<String> convierte(String palabra,int tipo) {
        String ruta ="";
        if(tipo==0)
            ruta ="Imagenes/";
        else if(tipo==1)
            ruta = "Videos/";
        else
            ruta="";
        ArrayList<String> tem = new ArrayList();
        StringTokenizer st = new StringTokenizer(palabra, ",");
        while (st.hasMoreTokens()) {
            tem.add(ruta+st.nextToken());
        }
        return tem;
    }
    
    /** Metodo para Guardar un juego en la base
    * @param juego - A guardar en la base
    * @return Boolean - Si se guardo con exito
    */
    private boolean guardaJuego(Videojuego juego) {

        controlJuego = new VideojuegoJpaController(utx, emf);
        {
            try {
                controlJuego.create(juego);
                return true;
            } catch (RollbackFailureException ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }
    
    /** Metodo para Guardar una imagen en la base
    * @param imagen- A guardar en la base
    * @return Boolean - Si se guardo con exito
    */
    private boolean guardaImagen(Imagen imagen) {

        controlImagen = new ImagenJpaController(utx, emf);
        {
            try {
                controlImagen.create(imagen);
                return true;
            } catch (RollbackFailureException ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }
    
     /** Metodo para Guardar un video en la base
    * @param video - A guardar en la base
    * @return Boolean - Si se guardo con exito
    */
    private boolean guardaVideo(Video video) {

        controlVideo = new VideoJpaController(utx, emf);
        {
            try {
                controlVideo.create(video);
                return true;
            } catch (RollbackFailureException ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }
    
     /** Metodo para limpiar las imagenes en la base
    * @param imagenes - A borrar
    * @return Boolean - Si se borro con exito
    */
    private boolean borraImagen(List<Imagen> imagenes) {

        controlImagen = new ImagenJpaController(utx, emf);
        ImagenPK img;
        Iterator<Imagen> iterador = imagenes.iterator();
        
        while(iterador.hasNext()){
            try {
                img = iterador.next().getImagenPK();
                controlImagen.destroy(img);
            } catch (RollbackFailureException ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    /** Metodo para limpiar las videos en la base
    * @param videos - A borrar
    * @return Boolean - Si se borro con exito
    */
    private boolean borraVideo(List<Video> videos){

        controlVideo = new VideoJpaController(utx, emf);
        VideoPK vid;
        
        Iterator<Video> iterador = videos.iterator();
        
        while(iterador.hasNext()){
            try {
                vid = iterador.next().getVideoPK();
                controlVideo.destroy(vid);
            } catch (RollbackFailureException ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    /** Metodo para clonar una listas de videos e imagenes
    * @param listaI - Imagenes a clonar
    * @param listaV - Videos a clonar
    * @return ArrayList - Contiene los objetos clonados
    */
    private ArrayList clone(List<Imagen> listaI,List<Video> listaV){
        
        Iterator<Imagen> iteratorimg = listaI.iterator();
        Iterator<Video> iteratorvid = listaV.iterator();
        ArrayList tem = new ArrayList();
        ArrayList tem1 = new ArrayList();
        
        if(listaI.size()==0)
            tem.add("");
        else
        while(iteratorimg.hasNext()){
             tem1.add(iteratorimg.next().getRutaarchivo());
            
        }
        tem.add(tem1);
        tem1 = new ArrayList();
        
        if(listaV.size()==0)
            tem.add("");
        else
        while(iteratorvid.hasNext())
            tem1.add(iteratorvid.next().getRutaarchivo());
        
        
        tem.add(tem1);
        return tem;
        
    }
    
    /** Metodo para Actualizar un Videojuego
    * @param juego - A actualizar
    * @param imagenes - Imagenes a actualizar
    * @param videos - videos a actualizar
    * @return Boolean si se actualizo correctamente
    */
    
    private boolean actualiza(Videojuego juego,String imagenes,String videos){
         
        int id = juego.getId();
        Videojuego tem = recuperaJuego(id) ;
        juego.setAlumnoList(tem.getAlumnoList());
        juego.setImagenList(tem.getImagenList());
        juego.setVideoList(tem.getVideoList());
        juego.setDesarrolladorList(tem.getDesarrolladorList());
        juego.setEjecutable(tem.getEjecutable());
        controlJuego = new VideojuegoJpaController(utx, emf);
        
         {
              try {
                        controlJuego.edit(juego);
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    } catch (RollbackFailureException ex) {
                        Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    } catch (Exception ex) {
                        Logger.getLogger(AdministradorDesarrolador.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    }
                }
        ArrayList<String> img = convierte(imagenes,2);
        ArrayList<String> vid = convierte(videos,2); 
        
        borraImagen(juego.getImagenList());
        borraVideo(juego.getVideoList());
            
        Imagen img1;

                for (int i = 0; i < img.size(); i++) {
                    img1 = new Imagen(id, i + 1);
                    img1.setRutaarchivo("Imagenes/"+verifica(img.get(i)));
                    img1.setVideojuego(recuperaJuego(id));
                    guardaImagen(img1);
                    }
        
        Video vid1;

                for (int i = 0; i < vid.size(); i++) {
                    vid1 = new Video(id, i + 1);
                    vid1.setRutaarchivo("Videos/"+verifica(vid.get(i)));
                    vid1.setVideojuego(recuperaJuego(id));
                    guardaVideo(vid1);
                    }
        
        
        return true;
    }
    /** Metodo para Verificar si la ruta ya estaba guardad en la base / 
    * si es asi quita / si no se queda igual
    * @param ruta - A procesar
    * @return String con la ruta sin /
    */
    
    private String verifica(String ruta){
        if(ruta.contains("/")){
            return ruta.substring(ruta.indexOf("/")+1,ruta.length());
        }else
            return ruta;
        
    }
    
}
