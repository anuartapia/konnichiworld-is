package controlador;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import modelo.Alumno;
import modelo.AlumnoJpaController;
import modelo.Videojuego;
import modelo.VideojuegoJpaController;
import modelo.exceptions.RollbackFailureException;

/**
 * Controlador que se comunica con los modelos <code>AlumnoJpaController</code>
 * y <code>VideojuegoJpaController</code> para verificar los datos de un
 * <code>Alumno</code> en la base de datos y determinar si puede obtener un
 * <code>Videojuego</code>, además actualiza los datos del <code>Alumno</code>
 * en caso de que sea una nueva adquisición.
 *
 * @version 1 19 Abr 2015
 * @author anuar
 */
public final class Descargador {

    /**
     * Indica que un <code>Alumno</code> puede obtener un
     * <code>Videojuego</code>.
     */
    public static final int OK = 0;

    /**
     * Indica que no se encuentra un <code>Videojuego</code> en la base de
     * datos.
     */
    public static final int NO_EXISTE_VIDEOJUEGO = 1;

    /**
     * Indica que un <code>Alumno</code> no cuenta con crédito suficiente para
     * adquirir un <code>Videojuego</code>.
     */
    public static final int NO_TIENE_CREDITO = 2;

    /**
     * Controlador JPA del modelo <code>Alumno</code>.
     */
    private final AlumnoJpaController ajc;

    /**
     * Controlador JPA del modelo <code>Videojuego</code>.
     */
    private final VideojuegoJpaController vjc;

    /**
     * Instancia que deberá contener los datos del <code>Alumno</code> obtenidos
     * a partir de su id.
     */
    private final Alumno alumno;

    /**
     * Regresa el valor de <code>alumno</code>
     *
     * @return <code>Alumno</code> con el valor actual de <code>alumno</code>.
     */
    public Alumno getAlumno() {
        return alumno;
    }

    /**
     * Instancia que deberá contener los datos del <code>Videojuego</code>
     * obtenidos a partir de su id.
     */
    private final Videojuego videojuego;

    /**
     * Regresa el valor de <code>videojuego</code>
     *
     * @return <code>Videojuego</code> con el valor actual de <code>videojuego</code>.
     */
    public Videojuego getVideojuego() {
        return videojuego;
    }

    /**
     * Constructor que inicializa <code>ajc</code> y <code>vjc</code> con los
     * parámetros <code>emf</code> y <code>utx</code>. A partir de esos
     * controladores consulta la base de datos para inicializar
     * <code>alumno</code> y <code>videojuego</code> con los dentificadores que
     * se indican.
     *
     * @param idAlumno - Identificador del <code>Alumno</code>.
     * @param idVideojuego - Identificadore del <code>Videojuego</code>.
     * @param emf - Manejador de entidades.
     * @param utx - Unidad de transacciones.
     */
    public Descargador(String idAlumno, int idVideojuego, EntityManagerFactory emf, UserTransaction utx) {
        ajc = new AlumnoJpaController(utx, emf);
        vjc = new VideojuegoJpaController(utx, emf);
        alumno = obtenAlumno(idAlumno);
        videojuego = obtenVideojuego(idVideojuego);
    }

    /**
     * Regresa el <code>Alumno</code> cuyo id es el correo que le pasan. Regresa
     * null si no existe en la base de datos.
     *
     * @param correo - Correo del <code>Alumno</code>.
     * @return <code>Alumno</code> al que corresponde el <code>correo</code>.
     */
    public Alumno obtenAlumno(String correo) {
        if (correo == null || correo.equals("")) {
            return null;
        } else {
            return ajc.findAlumno(correo);
        }
    }

    /**
     * Regresa el <code>Videojuego</code> cuyo id es el número que le pasan.
     * Regresa null si no existe en la base de datos.
     *
     * @param id - Identificador del <code>Videojuego</code>.
     * @return <code>Videojuego</code> al que corresponde el <code>id</code>.
     */
    public Videojuego obtenVideojuego(int id) {
        return vjc.findVideojuego(id);
    }

    /**
     * Determina si <code>alumno</code> puede descargar <code>videojuego</code>.
     *
     * @return <code>OK</code> si la respuesta es afirmativa,
     * <code>NO_EXISTE_VIDEOJUEGO</code> si no existe <code>videojuego</code> y
     * <code>NO_TIENE_CREDITO</code> si <code>alumno</code> no tiene crédito
     * mayor o igual a <code>videojuego</code>.
     */
    public int puedeDescargar() {
        if (videojuego == null) {
            return NO_EXISTE_VIDEOJUEGO;
        } else {
            if (alumno == null) {//sesion no iniciada
                System.out.println("alumno null");
                if (videojuego.getCosto() == 0) {//el juego es gratis
                    return OK;
                } else {
                    return NO_TIENE_CREDITO;
                }
            } else {//hay que verificar si tiene credito o si ya lo tiene en su colección
                if (alumno.getVideojuegoList().contains(videojuego)) {//ya ha comprado el videojuego
                    return OK;
                } else {
                    if (alumno.getCredito() >= videojuego.getCosto()) {//tiene credito suficiente
                        return OK;
                    } else {
                        return NO_TIENE_CREDITO;
                    }
                }
            }
        }
    }

    /**
     * Regresa un arreglo de bytes, que representa el archivo ejecutable de
     * <code>videojuego</code>.
     *
     * @return Archivo ejecutable de <code>videojuego</code>.
     */
    public byte[] obtenArchivo() {
        if (puedeDescargar() == OK) {//se puede hacer la descarga
            if (alumno == null) {//no existe el alumno (sesion no iniciada) pero videojuego es gratis
                return videojuego.getEjecutable();
            } else {
                if (alumno.getVideojuegoList().contains(videojuego)) {//alumno ya ha comprado el videojuego
                    return videojuego.getEjecutable();
                } else {//hay que actualizar sus datos
                    alumno.setCredito(alumno.getCredito() - videojuego.getCosto());//descuento
                    //relaciona el alumno y el videojuego:
                    alumno.getVideojuegoList().add(videojuego);//agrega el videojuego a la colección del alumno
                    videojuego.getAlumnoList().add(alumno);//agrega el alumno a la colección del videojuego
                    try {//refleja los cambios en la base de datos
                        ajc.edit(alumno);
                        vjc.edit(videojuego);
                    } catch (RollbackFailureException ex) {
                        Logger.getLogger(Descargador.class
                                .getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(Descargador.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                    return videojuego.getEjecutable();
                }
            }
        } else {//no se puede realizar la decarga
            return null;
        }
    }
}
