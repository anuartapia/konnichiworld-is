<%-- 
    Document   : cuentaD
    Created on : 20-abr-2015, 12:25:04
    Author     : tania
--%>

<%@page import="modelo.Desarrollador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
Desarrollador d=(Desarrollador)session.getAttribute("desarrollador");
%>
<!DOCTYPE html>
<html>
    
       <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="estilo/bootstrap-3.3.4-dist/css/bootstrap.min.css">
        <link href="estilo/bootstrap-3.3.4-dist/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="estilo/css/miCss.css">
        <link href="estilo/bootstrap-fileinput-master/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="estilo/bootstrap-fileinput-master/js/fileinput.min.js" type="text/javascript"></script>
        
        <title>Desarrollador</title>
    </head>
    <body>
        <%@include file="navegador.jsp"  %>
       
        <div class="row container" style=" padding: 10px;
	margin: -30px 10px 10px 200px;">
        <h2 class = "container-fluid" style="border-bottom: 2px cyan">Desarrollador</h2>
        <h3>Aquí puedes cambiar los datos de tu cuenta y otrogar crédito a los amlumnos.</h3>
        <div class="row-fluid container-fluid ">

<!-- Este es el Formulario -->

<div class="panel ">

<ul class="list-group">
    <form action="CuentaDesarrollador" method="POST">
  <div class="form-group">
    
    
    <table>
    <tr>
    <td>
    <li class="list-group-item">
    <div class="form-group has-feedback">
        Nombre:<br>
        <input type="text" class="form-control" id="form2" aria-describedby="form2Status" name="Nombre" value=<%out.println(d.getNombre());%>>
    </div>
    </li>
    </td>
    
    <td>
    <li class="list-group-item">
    <div class="form-group has-feedback">
        Correo:<br>
        <input type="emal" class="form-control" id="form2" aria-describedby="form2Status" name="Ap" value=<%out.println(d.getCorreo());%>>
    </div>
    </li>
    </td>
    </tr>
    
    <tr>
   
<tr>
    <td>
    <li class="list-group-item">
    <div class="form-group  has-feedback">
        Contraseña:<br>
        <input type="password" class="form-control" id="form4" aria-describedby="form5Status" name="contrasena" value=<%out.println(d.getContrasena());%>>
    </div>
    </li>
    </td>
    
    <td>
    <li class="list-group-item">
    <div class="form-group  has-feedback">
        
  <br>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Cambiar contraseña</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cambiar contraseña</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  
  
</div>
    </div>
    </li>
    </td>
</tr>
<tr>
    <td>
    <li class="list-group-item">
    <div class="form-group">
    <input id="file-1" type="file" class="file" multiple=true>
    </div>
    </li>
    </td>
</tr>
    </tr>
    <tr>
        
                    <th>
                        Nota: Recuerda que sólo se otorga crédito una vez por semestre.
                    </th>
    </tr>
    
    </table>
  </div>
    
  <li class="list-group-item"> 
  <input type="submit" class="btn btn-success" value="Guardar">
  <a href="index.jsp" data-toggle="tooltip" data-placement="top" title="Regresa al inicio ;)"><button type="button" class="btn btn-warning">Salir</button></a>
  <button type="button" class="btn btn-danger" >Eliminar Cuenta</button>
  <a href="otorgacredito.jsp" data-toggle="tooltip" data-placement="top" title="Regresa al inicio ;)"><button type="button" class="btn btn-default">Otorgar crédito</button></a>

  </li>
</form>
  </ul>

</div>
        <%--        
<script src="estilo/jquery.js"></script>
<script src="estilo/bootstrap-modal.js"></script>
        --%>
<script>
$("#file-1").fileinput({
showCaption: false,
browseClass: "btn btn-primary btn-lg",
fileType: "any"
});
</script>

    </body>
</html>