<%-- 
    Document   : catalogo
    Created on : Mar 10, 2015, 2:38:22 PM
    Author     : anuar
--%>

<%@page import="java.util.Iterator"%>
<%@page import="modelo.Videojuego"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="estilo/bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="estilo/css/miCss.css">
        <title>Catálogo</title>
    </head>
    <body>
        <%@include file="navegador.jsp"  %>
        <%
            /* obtiene una lista con todos los videojuegos en el sistema */
            List<Videojuego> lista = (List<Videojuego>) session.getAttribute("todos_los_juegos");
            /* iterador sobre la lista */
            Iterator<Videojuego> it = lista.iterator();
            /* instancia auxiliar de Videojuego */
            Videojuego v;
        %>
        <div class="panel panel-default">
            <%
                /* mientras que haya elementos en el iterador */
                while (it.hasNext()) {
                    /* siguiente Videojuego en la lista */
                    v = it.next();
                    /* despliega los datos de v*/
            %>
            <table style="border-bottom: 2px solid cyan;">
                <tr style="border-bottom: 1px solid cyan;background-color:Lavender;">
                    <th style="font-size: 150%;background-color:Lavender;">
                <form class="navbar-form navbar-left" method="post" action="DetalleJuegoIH">
                    <input type="text" id="form1" name="idjuego" value="<%=v.getId()%>" style="display: none">
                    <input type="submit" name="entrar" class="text-center" value="<%=v.getNombre()%>" />
                </form>
                </th>
                </tr>
                <tr>
                    <th><img src="http://www.pd4pic.com/images/joystick-classic-controller-icon-stick-cartoon.png" style="width:200px;high:200px;"></th>
                    <th><div class="panel-body"><%=v.getDescripcion()%></div></th>
                <th><div class="panel-body"><%=v.getCosto()%></div></th>
                </tr>
            </table>
        </div>
        <%            
                /* termina la iteración */
                }
        %>
    </body>
</html>
