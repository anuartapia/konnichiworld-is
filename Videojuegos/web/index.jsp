<%-- 
    Document   : inicio
    Created on : 15-abr-2015, 12:12:08
    Author     : tania
--%>

<%@page import="modelo.Desarrollador"%>
<%@page import="modelo.Alumno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Videojuegos</title>

        <link href="estilo/bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="estilo/css/miCss.css">

    </head>

    <body>
        <%@include file="navegador.jsp"  %>

        <div class="carrusel">

            <div id="myCarousel" class="carousel slide container cuadro" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="estilo/Imagenes/forza.jpg">
                    </div>

                    <div class="item">
                        <img src="estilo/Imagenes/gta.jpg">
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">Forza</div>
                <div class="panel-body">
                    <p>Pequeña reseña</p>



                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">GTA V</div>
                <div class="panel-body">Panel Content
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Panel Header</div>
                <div class="panel-body">Panel Content</div>
            </div>
        </div>
        <script src="estilo/jquery-2.1.3.js"></script>
        <script src="estilo/javascript/script.js"></script>
        <script src="estilo/bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
    </body>

</html>
