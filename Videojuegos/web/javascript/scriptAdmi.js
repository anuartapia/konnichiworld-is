//Variables Globales
var juego = false;
var portada = false;
var video = false;
var imagenes = [];
var videos = [];

/** Funcion para Ejecitar despues de cargar DOM
*/

$(document).ready(function(){
    
$("#descripcion").val("");

/** Funcion que al precionar una tecla en el buscador manda petición
 *  via Ajax al servlet AdministradorDesarrolador.java
 *  para recuperar lista de nombre de juegos
 */

$("#id1").on('input',function() {
     if($("#id1").val()==""){
        limpia();
        return;
    }
    $("#archivos span:first-child").attr("disabled","true");
    $(".rutaArchivo").text("");
   
    $.ajax({
            url : 'Administrador',
            type: "Post",
            datatype: "json",
            data : {operacion : "listar",letra : $('#id1').val()},
            
            error: function(data){
                console.log(data.status + " " + data.statusText);
            },
            success : desplegarLista
        });
    }
);

/** Funcion que al precionar una un elemento de la lista lanza una peticion
 *  via Ajax al servlet AdministradorDesarrolador.java
 *  para recuperar los datos del juego solicitado
 */

$(".lista").on("click",function(){
       
       /*
        if(juego||portada||video){
         alert("Hay una operacion en proceso");
         return;
        }
    */
        var id = $(this).data("id");
	console.log("Este es el id oculto"+id); 
        limpia();
        imagenes=[];
        videos=[];
        $("#archivos span:first-child").attr("disabled","true");
       
   	$.ajax({
            url : "Administrador",
            type: "Post",
            data : {operacion : "buscaJuego", id : id },
            
            error: function(data){
                console.log(data.status + " " + data.statusText);
            },
            success : ponerValores
        });
});

/** Funcion para quitar la ruta al seleccionar un archivo
 */

$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
});

/** Funcion que debugear archivo seleccionado
 */
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        console.log(numFiles);
        console.log(label);
    });

/** Funcion que quita ruta del videojuego seleccionado
 */
    $("#juego").on('change',function(event){
       
       var nombre = $('#juego').val().replace(/\\/g, '/').replace(/.*\//, ''); 
        $(".rutaArchivo").text(nombre);
         $("#archivos span:first" ).removeClass("btn-success");
         $("#archivos span:first" ).addClass("btn-warning");       
         $("#juegoS").text("Sobre Escribir juego");
       
     
    });

    var borra = "<button type='button' class='elimina btn btn-danger' aria-label='Left Align'>";
        borra +=  "<span class='glyphicon glyphicon glyphicon-trash' aria-hidden='true'></span></button>"
    
    
/** Funcion que despliega las imagenes seleccionadas
 */    
   $("#portada").on('change',function(event){
       
       var nombre = $('#portada').val().replace(/\\/g, '/').replace(/.*\//, '');
       var tag = "<li class='list-group-item'>";
       var tagCierre = "</li>";
       var reader = new FileReader();
       var file = $('#portada')[0].files[0]
       if(file){
       var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onloadend = function(evt){
         $("#archivo2l").append(tag+"<img class = 'despliegaChico' src = "+reader.result+" >"+borra+tagCierre);
         $("#archivo2l li:last-child").data("nombre",nombre); 
         
      }
     }
    });

/** Funcion elimina imagenes de la lista
 */

    $('#archivo2l').on('click', '.elimina', function() {
         $(this).parent().remove(); 
        
         });
         
/** Funcion que despliega los videos seleccionadas
 */    

    
  $("#video").on('change',function(){
      
       var nombre = $('#video').val().replace(/\\/g, '/').replace(/.*\//, '');
       var tag = "<li class='list-group-item'>";
       var tagCierre = "</li>";
       var reader = new FileReader();
       var file = $('#video')[0].files[0]
       if(file){
       var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function(evt){
         var entradaVideo = "<video autoplay loop class='embed-responsive-item despliegaChico'><source src="+reader.result+" type=video/mp4>    </video>"; 
         $("#archivo3l").append(tag+entradaVideo+borra+tagCierre);
         $("#archivo3l li:last-child").data("nombre",nombre); 
         }
     }
    });
    
/** Funcion elimina videos de la lista
 */

     $('#archivo3l').on('click', '.elimina', function() {
         $(this).parent().remove(); 
        
         });
    

   $("#limpiar").click(limpia);

/** Funcion verifica los datos introducidos y los envia medio Ajax al servlet
 * AdministradorDesarrolador.java
 */

  $("#confirmar").click(function(){
    
    cargaArray();
    console.log("Imagenes :"+imagenes.toString());
    console.log("videos : "+videos.toString());
    
    if(juego||portada||video){
       alert("Hay una operacion en proceso");
        return;
    }
    
    if(!verifica()){
        return;
    }
    if($("#id").val()!=""){
        actualiza();
        return;
    }
    
    var reader = new FileReader();
    
    var file = $('#juego')[0].files[0]
    if(file){
        
    var reader = new FileReader();
    reader.readAsBinaryString(file);
    
    reader.onload = function(evt){
   
    var tam = file.size;
    var actual = false;
    var valor = parseInt(tam/100);
    var inicio = -1;
    var fin = 0;
    var bytes = "";
    var i=0;
    mandaSize(file.size);
    juego=true;
    while(!actual){ 
        
    if(fin+valor<tam){
    inicio=fin+1;
    fin+=valor;
    }else{
        inicio = fin+1;
        fin += tam-fin
        actual = true;
    }        
    bytes = reader.result.substring(inicio,fin);
      
       $.ajax({
            url : 'Administrador',
            type: "Post",
            datatype: "json",
            data : {operacion : "creaJuego" , bits : bytes , i: inicio, f:fin },
            
            error: function(data){
                console.log(data.status + " " + data.statusText);
            },
            success : function(respuesta){
                console.log(respuesta);
            }
        });
    }
    guardaJuego();
    }
    }else
    alert("No agrego ningun juego");
  
 });
   });
 
 /** Funcion retrollamada de para colocar los valores del juego 
  * @param data - Objeto que contiene los valores a colocar 
  *
 */

  function ponerValores(data){
    
    console.log("Regreso de buscar juego "+data);
    var jsonArray = jQuery.parseJSON(data);
    
    var json = jsonArray[0];
    
    $("#id").val(json.id);
    $("#nombre").val(json.nombre);
    $("#ano").val(json.fecha);
    $("#des").val(json.desarrollador);
    $("#costo").val(json.costo);
    $("#descripcion").val(json.descripcion);
    $("#juegoS").text("Sobre Escribir juego");
    $("#portadaS").text("Sobre Escribir portada");
    $("#videoS").text("Sobre Escribir video");
    
    
    
    $("#archivos span:nth-child(n)" ).removeClass("btn-success");
    $("#archivos span:nth-child(n)" ).addClass("btn-warning");       
    
    $("#archivos i:nth-child(n)" ).removeClass("glyphicon-plus");
    $("#archivos i:nth-child(n)" ).addClass("glyphicon-upload");
    
    var tag = "<li class='list-group-item'>";
    var tagCierre = "</li>";
    
    var borra = "<button type='button' class='elimina btn btn-danger' aria-label='Left Align'>";
        borra +=  "<span class='glyphicon glyphicon glyphicon-trash' aria-hidden='true'></span></button>"
    for(i=0;i<jsonArray[1].length;i++){
       $("#archivo2l").append(tag+"<img class = 'despliegaChico' src = "+jsonArray[1][i]+" >"+borra+tagCierre);
       console.log(jsonArray[1][i]);
         $("#archivo2l li:last-child").data("nombre",jsonArray[1][i]); 
    }
    
     for(i=0;i<jsonArray[2].length;i++){
       $("#archivo3l").append(tag+"<img class = 'despliegaChico' src = "+jsonArray[2][i]+" >"+borra+tagCierre);
       $("#archivo3l li:last-child").data("nombre",jsonArray[2][i]); 
    }
    
    
}
/** Funcion borra todos los datos del formulario y listas de juego, 
 * video e imagenes.
 */

  function limpia(){
    
    $("#id").val("");
    $("#nombre").val("");
    $("#ano").val("");
    $("#des").val("");
    $("#costo").val("");
    $("#descripcion").val("");
    $("#juegoS").text("Añadir juego");
    $("#portadaS").text("Añadir portada");
    $("#videoS").text("Añadir video");
    
    $("#archivo1").empty();
    $("#archivo2l").empty();
    $("#archivo3l").empty();
    
    $("#archivo1").removeClass("progress");
    
    $("#archivos span:nth-child(n)" ).removeClass("btn-warning");           
    $("#archivos span:nth-child(n)" ).addClass("btn-success");
    
    $("#archivos i:nth-child(n)" ).removeClass("glyphicon-upload");       
    $("#archivos i:nth-child(n)" ).addClass("glyphicon-plus");
    
    $("#archivos span:first-child").removeAttr("disabled");
    $(".rutaArchivo").text("");
    $(".lista").each(function(){$(this).text("")});
    imagenes = [];
    videos = [];
}

/** Funcion simula una barrra de progreso a subir un videojuego
 * @param elemento - donde se insertara la barra de progreso
 */

  function creaBarradeProgreso(elemento){
   
   elemento.addClass("progress");
   
   var cadena = "<div class ='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' \n\
       aria-valuemax='100' style='width: 0%;'>";
   cadena += "0%</div>";
   
   elemento.append(cadena);
   
}

/** Funcion actualiza la barrra de progreso 
 * @param elemento - donde se insertara la barra de progreso actualizada
 * @param valor - que se pondra en la barra
 */

  function actualizarBarra(elemento,valor){
    if(valor<=100){
    $(elemento).css('width', valor+'%');
    $(elemento).empty();
    $(elemento).append(valor+'%');
    if(valor>30){
        $(elemento).removeClass("progress-bar-danger");
        $(elemento).addClass("progress-bar-warning");
        if(valor>75){
        $(elemento).removeClass("progress-bar-warning");
        $(elemento).addClass("progress-bar-success");
        }
    }
  }
       
}

/** Funcion sube juego por post al servlet AdministradorDesarrollador
 */

  function subeJuego(){
     console.log("Estoy en la funcion Juego");    

var reader = new FileReader();
    
    var file = $('#juego')[0].files[0]
      if(file){
        
    var reader = new FileReader();
    reader.readAsBinaryString(file);
    
    reader.onload = function(evt){
    enviarBytes("juego",reader.result,file.size);
    }
      }
    subeportada();
  }
 
 /** Funcion sube imagenes por post al servlet AdministradorDesarrollador
 */


  function subeportada(){ 
console.log("Estoy en la funcion portada");    

var reader = new FileReader();
    
    var file = $('#portada')[0].files[0]
      if(file){
        
    var reader = new FileReader();
    reader.readAsBinaryString(file);
    
    reader.onload = function(evt){
    enviarBytes("portada",reader.result,file.size);
    }
      }
    subevideo();
  }

/** Funcion envia bytes a servlet
 */

  function enviarBytes(tipo,binaryString,tam){
    
    var elemento="";
    if(tipo=="juego")
        elemento = $("#archivo1 > div");
    if(tipo=="portada")
        elemento = $("#archivo2 > div");
    if(tipo=="video")
        elemento = $("#archivo3 > div");
    //console.log(binaryString);
    var actual ="incompleto";
    var valor = parseInt(tam/100);
    var inicio = -1;
    var fin = 0;
    var bytes = "";
    var i=0;
    
    $.ajax({
            url : 'Administrador',
            type: "Get",
            data : {operacion : "buffer" , size : tam},
            
            error: function(){
                
            },
            success : function(res){
                console.log(res);
            }
        });
    
    while(actual=="incompleto"){ 
        
    if(fin+valor<tam){
    inicio=fin+1;
    fin+=valor;
    }else{
        inicio = fin+1;
        fin += tam-fin
        actual = "completo";
    }        
    bytes = binaryString.substring(inicio,fin);
      
       $.ajax({
            url : 'Administrador',
            type: "Post",
            datatype: "json",
            data : {operacion : "subir"+tipo , estado : actual ,bits : bytes , i: inicio, f:fin},
            
            error: function(data){
                console.log(data.status + " " + data.statusText);
            },
            success : function(respuesta){
                actualizarBarra(elemento,i++);    
            }
        });
    }
  }
  
  /** Funcion de retrollamada de buscar videojuego
   * @param json - string con formato json 
 */

  function desplegarLista(json){
      console.log(json);
      
      var arreglo = jQuery.parseJSON(json);
      
      for(i=0;i<arreglo.length;i++){
     
          $("#"+i).text(arreglo[i][1]);
          $("#"+i).data("id",arreglo[i][0]);
      }
      for(j=arreglo.length;j<5;j++)
          $("#"+j).empty();
      
  }
  /** Funcion pide juego al servelt via post
   * @param ID- Del videojuego a recuperar
 */

  function recuperaJuego(ID){
      $.ajax({
            url : 'Administrador',
            type: "Post",
            datatype: "json",
            data : {operacion : "recupera",id : ID},
            
            error: function(data){
                console.log(data.status + " " + data.statusText);
            },
            success : ponerValores
        });
  }
 
/** Funcion sube imagen con un id del videojueo
 * @param id - Corresponde la imagen al videojuego
 */

  function subeImagen(id){
    console.log("Este es el id que subo "+id);  
    console.log("Estas son las imagenes"+imagenes.toString());
    if(imagenes.length>0){
    portada = true;    
    $.ajax({
            url : 'Administrador',
            type: "Post",
            datatype: "json",
            data : {operacion : "creaImagen",id : id , imagenes : imagenes.toString()},
            
            error: function(data){
                console.log(data.status + " " + data.statusText);
            },
            success : function(json){
                alert("regreso");
              console.log(json);
              var arreglo = jQuery.parseJSON(json);
               if(arreglo[0]=='1'){
                   console.log(1);
                    subeVideo(arreglo[1]) 
                      }else
                        alert(arreglo[0]); 
            }
        });
    
    }else
        alert("No entro");
   juego = portada = false;
  }
  
  /** Funcion sube video con un id del videojueo
 * @param id - Corresponde el video al videojuego
 */

  function subeVideo(id){
    
    if(videos.length>0){
    video = true;    
    $.ajax({
            url : 'Administrador',
            type: "Post",
            datatype: "json",
            data : {operacion : "creaVideo",id : id , videos : videos.toString()},
            
            error: function(data){
                console.log(data.status + " " + data.statusText);
            },
            success : function(json){
              console.log(json);
              var arreglo = jQuery.parseJSON(json);
               if(arreglo[0]=='1'){
                   console.log(1);
                      }else
                        alert(arreglo[0]); 
            }
        });
    
    }else
        alert("No entro");
   juego = portada = video = false;
  }
  
 /** Funcion manda el tamaño al servlet para inicializar el buffer
 * @param tam - tamaño del juego para crear el buffer en el servlet
 */

  function mandaSize(tam){
        $.ajax({
            url : 'Administrador',
            type: "Get",
            data : {operacion : "buffer" , size : tam},
          
            success : function(res){
                console.log(res);
            }
        });
   }
   
 /** Funcion que pone fin al juego para subir 
 */

  function guardaJuego(){
       creaBarradeProgreso($("#archivo1"));
    
        $.ajax({
            url : "Administrador",
            type: "Post",
            data : {operacion : "guardaJuego", nombre : $("#nombre").val(),
                                              fecha :  $("#ano").val(),
                                              desarrollador : $("#des").val(),
                                              costo :  $("#costo").val(),
                                              descripcion : $("#descripcion").val()
                                              //juego : reader.result,
                                              //size :  file.size
            },
            error: function(data){
                console.log(data.status + " " + data.statusText);
            },
            success : function(json){
              console.log(json);
              var arreglo = jQuery.parseJSON(json);
               if(arreglo[0]=='1'){
                  var  elemento = $("#archivo1 > div");
                       actualizarBarra(elemento,100);
                       subeImagen(arreglo[1]) 
                      }else
                        alert(arreglo[0]);  
            }
        });
   }
       
/** Funcion sube un juego para actualizar
 */
   
 function actualiza(){
    
     $.ajax({
            url : "Administrador",
            type: "Post",
            data : {operacion : "actualizaJuego", id : $("#id").val(),
                                                  nombre : $("#nombre").val(),
                                                  fecha :  $("#ano").val(),
                                                  desarrollador : $("#des").val(),
                                                  costo :  $("#costo").val(),
                                                  descripcion : $("#descripcion").val(),
                                                  imagenes : imagenes.toString(),
                                                  videos :   videos.toString()     
            },
            error: function(data){
                console.log(data.status + " " + data.statusText);
            },
            success : function(ok){
              alert(ok);
            }
        });
     
 } 
 /** Funcion verifica que el costo sea positivo
 * @return  Boolean - falso si es menor y verdadero en caso contrario
 */

 function verifica(){
     if(parseInt($("#costo").val())<0){
     return false;
     }else
     return true;
     
 }
 /** Funcion carga el array de imagenes y videos
 */

 function cargaArray(){
     imagenes = [];
     videos = [];
     $("ul#archivo2l > li").each(function(index,element){
        imagenes.push($(this).data("nombre"));
        console.log("Esto imprime "+imagenes.toString());
     }); 
     
     $("ul#archivo3l > li").each(function(index,element){
        videos.push($(this).data("nombre"));
     }); 
 }
 
    
