<%-- 
    Document   : inicio
    Created on : 15-abr-2015, 12:12:08
    Author     : tania
--%>
<%@page import="modelo.Desarrollador"%>
<%@page import="modelo.Alumno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    /* alumno dueño de la sesión */
    Alumno aluNav = (Alumno) session.getAttribute("alumno");
    /* desarrollador dueño d ela sesión */
    Desarrollador desNav = (Desarrollador) session.getAttribute("desarrollador");
    /* determina si la sesión pertenece a un alumno */
    boolean sesionAlu = (aluNav != null); //true si la sesion es activa y es de un alumno
    /* determina si la sesión pertenece a un desarrollador */
    boolean sesionDes = (desNav != null);
    /* determina si la sesión pertenece a alguien */
    boolean sesionIniciada = (sesionAlu || sesionDes);//true si hay una sesion activa
    /* cadenas auxiliares */
    String show;
    String url;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="estilo/bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="estilo/css/miCss.css">
    </head>
    <body>
        <br><br><br>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.jsp"><img src="estilo/Imagenes/logo.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <a class="navbar-brand" href="index.jsp">Inicio</a>
                    <button type="button" class="navbar-toggle collapsed">
                    </button>
                    <a class="navbar-brand" href="CatalogoIH">Catálogo</a>
                    <button type="button" class="navbar-toggle collapsed">
                    </button>
                    <%
                        show = sesionIniciada ? "none" : "inline";
                    %>
                    <a class="navbar-brand" href="registro.jsp" style="display: <%=show%>;">Registro</a>
                    <button type="button" class="navbar-toggle collapsed">
                    </button>
                    <%
                        show = sesionIniciada ? "inline" : "none";
                        url = sesionAlu ? "cuenta.jsp" : "cuentaD.jsp";
                    %>
                    <a class="navbar-brand" href="<%=url%>" style="display: <%=show%>;">Mi Cuenta</a>
                    <%
                        show = sesionDes ? "inline" : "none";
                    %>
                    <a class="navbar-brand" href="administradorDesarrolladorIH.jsp" style="display: <%=show%>;">Administrar Juegos</a>
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-search"></span></button>
                    </form>

                    <ul class="nav navbar-nav navbar-right">
                        <%
                            show = sesionIniciada ? "none" : "inline";
                        %>
                        <li>
                            <a href="entrar.jsp" style="display: <%=show%>;">
                                <span class="glyphicon glyphicon-log-in" style="size: 6in 6in"></span>
                                Entrar
                            </a>
                        </li>
                    </ul>
                </div>  
            </div>
        </nav>
    </body>
</html>