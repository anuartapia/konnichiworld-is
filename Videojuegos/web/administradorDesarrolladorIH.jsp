<%-- 
    Document   : Administrador
    Created on : 11-mar-2015, 20:13:07
    Author     : Jose
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="estilo/bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="estilo/css/cssAdmi.css">

        <title> Administrar Juegos  </title>

    </head>
    <body>
        <%@include file="navegador.jsp"  %>

        <div class="row container" style = "text-align: left; margin: 25px 5px 70px 5px;">
            <br>
            <h2 class = "container-fluid" >Administrar Juegos</h2>
        </div>


        <div class="row-fluid container-fluid ">

            <!-- Este es el Formulario -->

            <div class="panel ">

                <ul class="list-group">
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">


                            <table>
                                <tr>
                                    <td>
                                <li class="list-group-item">
                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" id="id" aria-describedby="form2Status" placeholder="ID" disabled>
                                    </div>
                                </li>
                                </td>

                                <td>
                                <li class="list-group-item">
                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" id="nombre" aria-describedby="form2Status" placeholder="Nombre">
                                    </div>
                                </li>
                                </td>

                                <td>
                                <li class="list-group-item">
                                    <div class="form-group  has-feedback">
                                        <input type="text" class="form-control" id="ano" aria-describedby="form1Status" placeholder="Fecha         YYYY/MM/DD">
                                    </div>
                                </li>
                                </td>
                                </tr>

                                <tr>
                                    <td>
                                <li class="list-group-item">
                                    <div class="form-group  has-feedback">
                                        <input type="text" class="form-control" id="costo" aria-describedby="form3Status" placeholder="$ Costo">
                                    </div>
                                </li>
                                </td>

                                <td>
                                <li class="list-group-item">
                                    <div class="form-group  has-feedback">
                                        <input type="text" class="form-control" id="des" aria-describedby="form4Status" placeholder="Desarrollador">
                                    </div>
                                </li>
                                </td>

                                </tr>
                            </table>

                            <li class="list-group-item">
                                <div class="form-group  has-feedback">
                                    <textarea id ="descripcion" class="form-control" rows="5" cols ="84" placeholder="Descripcion" style = "resize: none;">
        
                                    </textarea>
                                </div>
                            </li>


                            <div id = "archivos" style = "margin: 70px 0px 0px 14px">

                                <span class="btn btn-success btn-file">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span id = "juegoS">Añadir juego</span>
                                    <input id = "juego" type="file">
                                </span> <div class ="rutaArchivo"></div>
                                <div id="archivo1" style = "margin: 10px">      
                                </div>
                            </div>
                            <span class="btn btn-success btn-file ">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span id = "portadaS">Añadir Portada</span>
                                <input id = "portada" type="file" name="files[]" multiple>
                            </span>
                            <div id="archivo2" style = "margin: 10px">   
                                <ul id = "archivo2l" class="list-group">
                                </ul>
                            </div>


                            <span class="btn btn-success btn-file">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span id = "videoS" >Añadir video</span>
                                <input id = "video" type="file" name="files[]" multiple>
                            </span>

                            <div id="archivo3" style = "margin: 10px">  
                                <ul id = "archivo3l" class="list-group">
                                </ul>

                            </div>


                        </div>

                        </div>

                    </form>
                </ul>

            </div>
        </div>
        <!--Este es el buscador-->

        <div class="thumbnail panel panel-default pull-right coloca">
            <img class = "despliega" src="Imagenes/logo.png"><!--"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQrRn_vTgOjtgLWClZ9gI4yu2rc9sHQG26cYA3g6E34sc8OZO6c3g"-->
            <div class="caption">
                <div class="form-group has-feedback">
                    <form>
                        <input id="id1" type="text" class="form-control"  placeholder="Search">
                    </form>    
                    <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true" style = "background-color: #4B8DF9;"></span>
                </div>

                <table id = "table" class="table table-striped table-hover">
                    <tr>
                        <td id = "0" class="lista">
                        </td>
                    </tr>
                    <tr>
                        <td id = "1" class="lista">
                        </td>
                    </tr>
                    <tr>
                        <td id = "2" class="lista">
                        </td>
                    </tr>
                    <tr>
                        <td id = "3" class="lista">
                        </td>
                    </tr>
                    <tr>
                        <td id = "4" class="lista">
                        </td>
                    </tr>



                </table>
            </div>
        </div>


        <div style = "margin: 0px 0px 100px 300px"> 
            <button id="confirmar" type="button" class="btn btn-success">Submit</button> 
            <button id="limpiar" type="button" class="btn btn-warning">Clean</button>
        </div>

        <script src="estilo/jquery/jquery-2.1.3.js"></script>
        <script src="javascript/scriptAdmi.js"></script>
    </body>
</html>
