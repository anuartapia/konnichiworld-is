<%-- 
    Document   : Registro
    Created on : Mar 13, 2015, 1:37:51 PM
    Author     : esteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    if(null!=request.getAttribute("errorMessage"))
    {
        out.println(request.getAttribute("errorMessage"));
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="estilo/bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="estilo/css/cssRegistro.css">



        <title>Registro</title>
    </head>
    <body>
        <%@include file="navegador.jsp"  %>
        <div class="row container" style = "text-align: left; margin: 25px 5px 70px 5px;">
            <h2 class = "container-fluid" >Registro konnichi World</h2>
            <h3>Bienvenido... estas apunto de poder descubrir un mundo de Juegos!</h3>

            <div class="btn-group btn-group-sm navegacion" role="group">
                <button type="button" class="btn btn-default btn-sm " id="catalogo">
                    <span class=" glyphicon glyphicon-th-list"></span>
                </button>
                <button type="button" class="btn btn-default btn-sm" id="home">
                    <span class="glyphicon glyphicon-home"></span>
                </button>
                <button type="button" class="btn btn-default btn-sm " id="search">
                    <span class="glyphicon glyphicon-search"> </span>
                </button>
            </div>

            <div class="row-fluid container-fluid ">

                <!-- Este es el Formulario -->

                <div class="panel ">

                    <ul class="list-group">
                        <form action="NuevoAlumno" method="POST">
                            <div class="form-group">


                                <table>

                                    <tr>
                                        <td>
                                    <li class="list-group-item">
                                        <div class="form-group has-feedback">
                                            <input type="email" class="form-control" id="form6" aria-describedby="form6Status" placeholder="Correo" name="correo">
                                        </div>
                                    </li>
                                    </td>

                                    <td>
                                    <li class="list-group-item">
                                        <div class="input-group">
                                            <label>Fecha Nacimiento</label>
                                            <input type="text" class="btn btn-default btn-sm" name="dia" placeholder="Dia" size="4" maxlength="2"    onkeypress='return event.charCode >= 48 && event.charCode <= 57'></input>
                                            <input type="text" class="btn btn-default btn-sm" name="mes" placeholder="Mes" size="4"maxlength="2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></input>
                                            <input type="text" class="btn btn-default btn-sm" name="anio" placeholder="A&ntilde;o" size="5"maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></input>
                                        </div>
                                    </li>
                                    </td>


                                    </tr>

                                    <tr>
                                        <td>
                                    <li class="list-group-item">
                                        <div class="form-group has-feedback">
                                            <input type="text" class="form-control" id="form1" aria-describedby="form1Status"  maxlength="20" placeholder="Nombre(s)" name="nombre" onkeypress='return event.charCode >= 97 && event.charCode <= 122'>
                                        </div>
                                    </li>
                                    </td>

                                    <td>
                                    <li class="list-group-item">
                                        <div class="form-group  has-feedback">
                                            <input type="password" class="form-control" id="form5" aria-describedby="form5Status"  maxlength="10" placeholder="Contraseña" name="pwd1">
                                        </div>
                                    </li>
                                    </td>

                                    </tr>

                                    <tr>
                                        <td>
                                    <li class="list-group-item">
                                        <div class="form-group  has-feedback">
                                            <input type="text" class="form-control" id="form2" aria-describedby="form2Status" maxlength="15" placeholder="Apellido Paterno" name="apellidoPa"  onkeypress='return event.charCode >= 97 && event.charCode <= 122'>
                                        </div>
                                    </li>
                                    </td>

                                    <td>
                                    <li class="list-group-item">
                                        <div class="form-group  has-feedback">
                                            <input type="password" class="form-control" id="form4" aria-describedby="form4Status"  maxlength="10" placeholder="Repetir Contraseña" name="pwd2">
                                        </div> 
                                        <div style="color: #FF0000;" nidden="true" >${errorMessage}</div>
                                    </li>
                                    </td>

                                    <tr>
                                        <td>
                                    <li class="list-group-item">
                                        <div class="form-group  has-feedback">
                                            <input type="text" class="form-control" id="form3" aria-describedby="form3Status" maxlenhgt="15"  placeholder="Apellido Materno" name="apellidoMa"  onkeypress='return event.charCode >= 97 && event.charCode <= 122'>
                                        </div>
                                    </li>
                                    </td>

                                    <td>
                                    <li class="list-group-item">


                                        <select class="btn selectpicker btn-primary" name="semestre">
                                            <option>Semestre</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>

                                        </select>

                                    </li>
                                    </td>

                                    </tr>

                                    <tr>
                                        <td>
                                    <li class="list-group   -item">

                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <span class="btn btn-primary btn-file">
                                                    Cargar&hellip;
                                                    <input type="file" multiple>
                                                </span>
                                            </span>
                                            <input type="text" class="form-control" readonly>
                                        </div>
                                        <span class="help-block">
                                            Carga tu historial para poder obter créditos y descargar juegos de paga

                                        </span>

                                    </li>
                                    </td>
                                    </tr>

                                </table>
                            </div>

                            <li class="list-group-item"> 
                                <input type="submit" class="btn btn-success" value="guardar" onsubmit="return validaCampos()"> 
                                <a href="index.jsp" data-toggle="tooltip" data-placement="top" title="Regresa al inicio ;)">
                                <button type="button" class="btn btn-warning">Salir</button>
                                <button type="reset" class="btn btn-danger">Limpiar Campos</button>
                                </a>

                            </li>
                        </form>
                    </ul>

                </div>



                <%--        
        <script src="estilo/jquery.js"></script>
        <script src="estilo/bootstrap-modal.js"></script>
                --%>
                <script>
                    $("#file-1").fileinput({
                        showCaption: false,
                        browseClass: "btn btn-primary btn-lg",
                        fileType: "any"
                    });

                    function validaContrasena() {
                        var nc = document.getElementById("pwd1").value;
                        var cc = document.getElementById("pwd2").value;
                        var espacios = false;
                        var cont = 0;

                        while (!espacios && (cont < nc.length)) {
                            if (nc.charAt(cont) === " ")
                                espacios = true;
                            cont++;
                        }

                        if (espacios) {
                            alert("La contraseña no puede contener espacios en blanco");
                            return false;
                        }

                        if (nc.length === 0 || cc.length === 0) {
                            alert("Los campos de la contraseña no pueden quedar vacios");
                            return false;
                        }

                        if (nc !== cc) {
                            alert("Las contraseñas deben de coincidir");
                            return false;
                        } else {
                            alert("Todo esta correcto");
                            return true;
                        }

                    }
                    function validaCampos() {
                        var fecha = document.getElementById("form4").value;
                        if (fecha === "año")
                            alert("ingresa año");
                        return false;
                    }
                </script>




                </body>
                </html>

