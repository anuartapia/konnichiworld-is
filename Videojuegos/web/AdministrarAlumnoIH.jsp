<%-- 
    Document   : Cuenta
    Created on : 07-abr-2015, 10:25:45
    Author     : tania
--%>

<%@page import="java.util.Date"%>
<%@page import="modelo.Alumno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    
Alumno a=(Alumno)session.getAttribute("alumno");
int dia=a.getFecha().getDate();
int mes=a.getFecha().getMonth()+1;
int anio=a.getFecha().getYear();
%>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

   

</script>
<!DOCTYPE html>
<html lang="en">
    
       <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="estilo/bootstrap-3.3.4-dist/css/bootstrap.min.css">
        <link href="estilo/bootstrap-3.3.4-dist/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="estilo/css/miCss.css">
        <link href="estilo/bootstrap-fileinput-master/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="estilo/bootstrap-fileinput-master/js/fileinput.min.js" type="text/javascript"></script>
        
        <title>Mi Cuenta</title>
        <script type="text/javascript">
$(function() {
	$('#dob').datepicker({dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '-100:+0'});
});
</script>
    </head>
    <body>
        <%@include file="navegador.jsp"  %>
       
        <div class="row container" style=" padding: 10px;
	margin: -30px 10px 10px 200px;">
        <h2 class = "container-fluid" style="border-bottom: 2px cyan">Mi Cuenta</h2>
        <h3>Aquí puedes cambiar los datos de tu cuenta.</h3>
        <div class="row-fluid container-fluid ">

<!-- Este es el Formulario -->

<div class="panel ">

<ul class="list-group">
    <form action="CuentaControlador" method="POST" onsubmit="return validaCampos()">
  <div class="form-group">
    
    
    <table>
    <tr>
    <td>
    <li class="list-group-item">
    <div class="form-group has-feedback">
        Nombre:<br>
        <input type="text" class="form-control" id="form2" aria-describedby="form2Status" name="Nombre" value=<%out.println(a.getNombre());%>>
    </div>
    </li>
    </td>
    
    <td>
    <li class="list-group-item">
    <div class="form-group has-feedback">
        Apellido paterno:<br>
        <input type="text" class="form-control" id="form2" aria-describedby="form2Status" name="Ap" value=<%out.println(a.getAp());%>>
    </div>
    </li>
    </td>

    <td>
    <li class="list-group-item">
    <div class="form-group  has-feedback">
        Apellido materno:<br>
        <input type="text" class="form-control" id="form1" aria-describedby="form1Status" name="Am" value=<%out.println(a.getAm());%>>
    </div>
    </li>
    </td>
    </tr>
    
    <tr>
    <td>
    <li class="list-group-item">
    <div class="form-group  has-feedback">
        Correo:<br>
        <input type="text" class="form-control" id="form3" aria-describedby="form3Status" name="correo" value=<%out.println(a.getCorreo());%> disabled>
    </div>
    </li>
    </td>

    <td>
    <li class="list-group-item">
    
        Fecha de nacimiento:<br>
       <span style="display: inline;">Día</span><input type="text" class="form-control" id="form4" aria-describedby="form4Status" name="dia" value="<%out.print(dia);%>">
        <span style="display: inline;">Mes</span><input type="text" class="form-control" id="form4" aria-describedby="form4Status" name="mes" value="<%out.print(mes);%>">
        <span style="display: inline;">Año</span><input type="text" class="form-control" id="form4" aria-describedby="form4Status" name="elanio" value="<%out.print(anio);%>">
        
    
    </li>
    </td>
    
<tr>
    <td>
    <li class="list-group-item">
    <div class="form-group  has-feedback">
        
  <br>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Cambiar contraseña</button>
  
  
  

    </div>
    </li>
    </td>
</tr>

    
    <tr>
        
                    <th>
                        Nota: Recuerda que sólo se te otorga crédito una vez por semestre
                    </th>
    </tr>
    
    </table>
  </div>
    
  <li class="list-group-item"> 
  <input type="submit" class="btn btn-success" value="Guardar">
  <a href="index.jsp" data-toggle="tooltip" data-placement="top" title="Regresa al inicio ;)"><button type="button" class="btn btn-warning">Salir</button></a>
  

  </li>
    
    
</form>
  </ul>
    <form action="EliminarCuenta" method="GET" onsubmit="return eliminarCuenta()" style="display:inline">
        <input type="submit" class="btn btn-danger" value="Eliminar Cuenta" >
        </form>
    <br><br>
    <a href="historialIH.jsp" data-toggle="tooltip" data-placement="top" title="Agega un historial nuevo ;)"><button type="button" class="btn btn-warning">Agrega historial</button></a>
  
</div>
     <!-- Modal content-->
      <form action="CambiarContrasenia" method="POST" onsubmit="return validaContrasena()">
      
      <div class="modal" id="myModal">
    <div class="modal-header">
        <h3>Cambiar contraseña <span class="extra-title muted"></span></h3>
    </div>
    <div class="modal-body form-horizontal">
        <div class="control-group">
            <label for="new_password" class="control-label">Nueva Contraseña</label>
            <div class="controls">
                <input name="new_password" type="password" id="pwd1">
            </div>
        </div>
        <div class="control-group">
            <label for="confirm_password" class="control-label">Confirma Contraseña</label>
            <div class="controls">
                <input name="confirm_password" type="password" id="pwd2">
            
        </div>      
    </div>
    <div class="modal-footer">
        <button href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <input type="submit" class="btn btn-primary" id="password_modal_save">Guardar Cambios</button>
    </div>
</div>
          
      
    </div>
          </form>
     
        <%--        
<script src="estilo/jquery.js"></script>
<script src="estilo/bootstrap-modal.js"></script>
        --%>
<script>
$("#file-3").fileinput({
showCaption: false,
browseClass: "btn btn-primary btn-lg",
fileType: "any"
});

function validaContrasena(){
    var nc = document.getElementById("pwd1").value;
    var cc = document.getElementById("pwd2").value;
    var espacios = false;
    var cont = 0;
 
    while (!espacios && (cont < nc.length)) {
        if (nc.charAt(cont) == " ")
        espacios = true;
        cont++;
    }
 
    if (espacios) {
        alert ("La contraseña no puede contener espacios en blanco");
        return false;
    }
    
    if (nc.length == 0 || cc.length == 0) {
        alert("Los campos de la contraseña no pueden quedar vacios");
        return false;
    }
    
    if (nc != cc) {
        alert("Las contraseñas deben de coincidir");
        return false;
    } else {
        alert("Todo esta correcto");
        
        return true; 
    }
    
}
function validaCampos(){
    var fecha=document.getElementById("form4").value;
    if (fecha == "año"){
        alert ("Ingresa año");
        return false;
    }else{
        return true;
    }
}

function eliminarCuenta(){
    var r=confirm ("Está seguro de eliminar la cueta?");
    if (r == true) {
        return true;
    } else {
        return false;
    }
    
   }
</script>

    </body>
    
&nbsp;
</html>