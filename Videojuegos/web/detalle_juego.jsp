<%-- 
    Document   : juego_detalle
    Created on : Mar 10, 2015, 3:39:06 PM
    Author     : anuar
--%>

<%@page import="modelo.Videojuego"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        /* Videojuego que se quiere desplegar en esta pagina */
        Videojuego v = (Videojuego) session.getAttribute("juego");
    %>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="estilo/bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="estilo/css/miCss.css">
        <title><%=v.getNombre()%></title>
    </head>
    <body>
        <%@include file="navegador.jsp"  %>
        <div class="row container" style = "text-align: left; margin: 25px 5px 70px 5px;">
            <h2 class = "container-fluid" ><%=v.getNombre()%></h2>
            <div class="btn-group btn-group-sm navegacion" role="group">
                <button type="button" class="btn btn-default btn-sm " id="catalogo">
                    <span class=" glyphicon glyphicon-th-list"></span>
                </button>
                <button type="button" class="btn btn-default btn-sm" id="home">
                    <span class="glyphicon glyphicon-home"></span>
                </button>
                <button type="button" class="btn btn-default btn-sm " id="search">
                    <span class="glyphicon glyphicon-search"> </span>
                </button>
            </div>
        </div>
        <div class="container-fluid" style = "text-align: justify; margin: 25px 5px 70px 5px;">
            <p>
                <%=v.getDescripcion()%>
            </p>
            <p>
                <!--
                <img src=<%=v.getImagenList().toString()%> alt="Mountain View" style="width:200px;height:150px">
                -->
                <img src="http://farm7.staticflickr.com/6091/6390291839_0a3fefff20_o.jpg" alt="Mountain View" style="width:200px;height:150px">
            </p>
            Precio: <%=v.getCosto()%> <br>
            <form class="navbar-form navbar-left" method="post" action="DescargaIH">
                <input type="text" name="idjuego" class="btn btn-success" value="<%=v.getId()%>" style="display: none"/>
                <input type="submit" name="comprar" class="btn btn-success" value="Comprar"/>
            </form>
        </div>
    </body>
</html>
