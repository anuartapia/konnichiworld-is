# Inegniería de Software - Konnichiworld

Repositorio para el desarrollo del proyecto de Ingeniería de Software.

## Portal de Videojuegos

Sistema basado en web donde un desarrollador de vieojuegos puede
publicar sus productos para que estudiantes puedan descargarlos.

### Desarrolladores

* Tania Vega [almaw@ciencias.unam.mx](mailto:almaw@ciencias.unam.mx)
* José Vargas [josevb@ciencias.unam.mx](josevb@ciencias.unam.mx)
* Esteban Hernández [stebanhdz@ciencias.unam.mx](stebanhdz@ciencias.unam.mx)
* Anuar Tapia [anuartap@ciencias.unam.mx](mailto:anuartap@ciencias.unam.mx)

***

## Ambiente de ejecución

* Marco de trabajo y entorno integrado de desarrollo: *Netbeans IDE 8.0.2*
* Manejador de bases de datos: *PostgreSQL 9.3.1*
* Servidor de aplicaciones: *GlassFish Server 4.1*

Para que las funciones principales trabajen correctamente es necesario tener
una base de datos existente llamada *videojuegos* y contener las tablas
generadas por el *script* **tablas.sql** ubicado en el paquete *Videojuegos/src/modelo*.